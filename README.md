# tr181-dhcpv6s

TR181 DHCPv6 server manager plugin

This component implements and manages the Device.DHCPv6.Server datamodel described in [here](https://usp-data-models.broadband-forum.org/tr-181-2-12-0-usp.html#D.Device:2.Device.DHCPv6.Server.)
