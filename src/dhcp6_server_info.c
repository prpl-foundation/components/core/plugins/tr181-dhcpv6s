/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_transaction.h>

#include "dhcp6_server_info.h"
#include "dhcp6_option_info.h"
#include "dm_dhcp6.h"
#include "dhcp6_priv.h"

/**
 * @brief
 * Fill the IA_PD and/or IA_NA data model lists with a prefix instance variant coming from a netmodel query.
 *
 * @param prefix_entry Netmodel variant instance to extract data from.
 * @param iana_prefix IA_NA prefix list.
 * @param iana_sep IA_NA separation string to be put before the prefix is append.
 * @param iapd_prefix IA_PD prefix list.
 * @param iapd_sep IA_PD separation string to be put before teh prefix is append.
 * @param upstream Select if the interface is an upstream interface.
 */
static void fill_prefixes(amxc_var_t* prefix_entry,
                          amxc_string_t* iana_prefix,
                          const char** iana_sep,
                          amxc_string_t* iapd_prefix,
                          const char** iapd_sep,
                          bool upstream) {
    const char* origin = GET_CHAR(prefix_entry, "Origin");
    when_null_trace(origin, exit, WARNING, "Cannot get origin from prefix object.");
    const char* prefix = GET_CHAR(prefix_entry, "Prefix");
    when_null_trace(prefix, exit, WARNING, "Cannot get prefix address from prefix object.");

    if(upstream) {
        if((strcmp(origin, "Child") == 0) || (strcmp(origin, "AutoConfigured") == 0)) {
            amxc_string_appendf(iana_prefix, "%s%s", *iana_sep, prefix);
            *iana_sep = ",";
        }
        if((strcmp(origin, "PrefixDelegation") == 0) || (strcmp(origin, "AutoConfigured") == 0)) {
            amxc_string_appendf(iapd_prefix, "%s%s", *iapd_sep, prefix);
            *iapd_sep = ",";
        }
    } else {
        if((strcmp(origin, "Child") == 0) || (strcmp(origin, "AutoConfigured") == 0)) {
            amxc_string_appendf(iapd_prefix, "%s%s", *iapd_sep, prefix);
            *iapd_sep = ",";
        }
    }
exit:
    return;
}

/**
 * @brief
 * Callback function for the getIPv6Prefixes query.
 *
 * @param sig_name Signal name, not used.
 * @param data List of IPv6 prefixes for an IP interface.
 * @param priv Private server_info_t corresponding to the pool which is registered to the interface.
 */
static void interface_prefix_list_cb(UNUSED const char* const sig_name,
                                     const amxc_var_t* const data,
                                     void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    bool upstream = false;
    server_info_t* info = (server_info_t*) priv;
    amxd_trans_t trans;
    amxc_string_t iana_prefix;
    amxc_string_t iapd_prefix;
    const char* iana_sep = "";
    const char* iapd_sep = "";

    SAH_TRACEZ_INFO(ME, "Callback function for getIPv6Prefixes query called.");

    amxd_trans_init(&trans);
    amxc_string_init(&iana_prefix, MAX_PREFIXES_LENGTH);
    amxc_string_init(&iapd_prefix, MAX_PREFIXES_LENGTH);

    when_null_trace(data, exit, ERROR, "No data given.");
    when_null_trace(info, exit, ERROR, "Cannot get server info data.");
    when_null_trace(info->intf_path, exit, ERROR, "Cannot get interface path from server info data.");

    upstream = netmodel_hasFlag(info->intf_path, "upstream", NULL, netmodel_traverse_down);

    amxc_var_for_each(prefix_entry, data) {
        fill_prefixes(prefix_entry, &iana_prefix, &iana_sep, &iapd_prefix, &iapd_sep, upstream);
    }

    info->flags.bit.have_prefix = !amxc_string_is_empty(&iapd_prefix) || !amxc_string_is_empty(&iana_prefix);

    amxd_trans_select_object(&trans, info->obj);
    amxd_trans_set_value(csv_string_t, &trans, "IANAPrefixes", amxc_string_get(&iana_prefix, 0));
    amxd_trans_set_value(csv_string_t, &trans, "IAPDPrefixes", amxc_string_get(&iapd_prefix, 0));
    status = amxd_trans_apply(&trans, dhcp6_get_dm());
    when_failed_trace(status, exit, ERROR, "Unable to apply transaction.");

    sync_pool_status(info->obj);
exit:
    amxc_string_clean(&iana_prefix);
    amxc_string_clean(&iapd_prefix);
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Callback function for the getFirstParameter with the parameter NetDevName query.
 *
 * @param sig_name Signal name, not used.
 * @param data Name of the interface.
 * @param priv Private server_info_t corresponding to the pool which is registered to the interface.
 */
static void interface_name_cb(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              void* const priv) {
    char* intf_name = NULL;
    server_info_t* info = (server_info_t*) priv;
    const amxc_llist_t* addr_list = NULL;
    amxc_var_t* addr = NULL;

    SAH_TRACEZ_INFO(ME, "Callback function for getFirstParameter query called.");

    when_null_trace(data, exit, ERROR, "Data is null.");
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the pool.");
    when_null_trace(info->obj, exit, ERROR, "Unable to retrieve the pool object.");

    addr_list = amxc_var_constcast(amxc_llist_t, data);
    when_false_trace(!amxc_llist_is_empty(addr_list), exit, ERROR, "No lla addr.");

    addr = amxc_var_from_llist_it(amxc_llist_get_first(addr_list));

    intf_name = amxc_var_dyncast(cstring_t, GET_ARG(addr, "NetDevName"));
    when_null_trace(intf_name, exit, ERROR, "Cannot get the interface name from NetModel.");
    free(info->intf_name);
    info->intf_name = intf_name;
    info->flags.bit.intf_exist = 1;

    sync_pool_status(info->obj);
exit:
    return;
}

static void dhcpv6_dns_change_cb(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    amxc_string_t dynamic_addrs;
    option_23_info_t* option_info = (option_23_info_t*) priv;

    when_null_trace(option_info, exit, ERROR, "No option info given to the lla or gua callback");

    amxc_string_init(&dynamic_addrs, 0);

    amxc_var_for_each(addr, data) {
        const char* type_flags = GET_CHAR(addr, "TypeFlags");
        if(option_info->expected_flags.bit.lla_ok && (strstr(type_flags, "@lla") != NULL)) {
            amxc_string_appendf(&dynamic_addrs, "%s,", GET_CHAR(addr, "Address"));
            option_info->flags.bit.lla_ok = 1;
        } else if(option_info->expected_flags.bit.gua_ok && (strstr(type_flags, "@gua") != NULL)) {
            amxc_string_appendf(&dynamic_addrs, "%s,", GET_CHAR(addr, "Address"));
            option_info->flags.bit.gua_ok = 1;
        }
    }

    free(option_info->dynamic_addr);
    option_info->dynamic_addr = amxc_string_take_buffer(&dynamic_addrs);

    dm_mngr_update_option_23(amxd_object_get_parent(option_info->obj), option_info->obj);
exit:
    amxc_string_clean(&dynamic_addrs);
    return;
}

/**
 * @brief
 * Clear the data allocated in a server_info structure
 *
 * @param info The server_info data to clear.
 */
void server_info_clear(server_info_t* info) {
    when_null_trace(info, exit, WARNING, "Info given is null");

    amxd_object_for_all(info->obj, ".Option.*", option_info_clean, info);

    if(info->q_prefix != NULL) {
        netmodel_closeQuery(info->q_prefix);
        info->q_prefix = NULL;
    }
    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
    }
    if(info->obj != NULL) {
        info->obj->priv = NULL;
    }

    amxc_var_delete(&(info->last_client_htable));
    info->last_client_htable = NULL;

    free(info->intf_path);
    free(info->intf_name);
    free(info->fw_alias);
    free(info);
exit:
    return;
}

/**
 * @brief
 * Disable the subscription to queries and remove the interface path from local private data.
 *
 * @param obj The amxd pool object.
 */
void server_info_disable(amxd_object_t* obj) {
    server_info_t* info = NULL;

    SAH_TRACEZ_INFO(ME, "Disabling NetModel query subscription.");

    when_null_trace(obj, exit, ERROR, "No pool object given.");
    info = (server_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "Cannot get the private server info from pool object.");

    if(info->q_prefix != NULL) {
        netmodel_closeQuery(info->q_prefix);
        info->q_prefix = NULL;
    }
    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
    }

    amxd_object_for_all(info->obj, ".Client.*", dm_remove_client, info);
    amxc_var_clean((info->last_client_htable));

    free(info->intf_path);
    info->intf_path = NULL;
    free(info->intf_name);
    info->intf_name = NULL;
    info->flags.all_flags = 0;
exit:
    return;
}

/**
 * @brief
 * Open all netmodel queries for the dhcpv6 server
 *
 * @param obj The dhcpv6 pool server amxd object.
 */
static void server_info_open_queries(amxd_object_t* obj) {
    server_info_t* info = NULL;
    char* intf_path = NULL;

    when_null_trace(obj, exit, ERROR, "Cannot get the pool object.");
    when_null_trace(obj->priv, exit, ERROR, "Cannot get private server info data from pool object.");
    info = (server_info_t*) obj->priv;

    intf_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    if(info->intf_path != NULL) {
        when_false_trace(strcmp(intf_path, info->intf_path) != 0, exit, INFO, "Interface did not change, not updating queries");
    }

    free(info->intf_path);
    info->intf_path = intf_path;

    if(info->q_prefix != NULL) {
        netmodel_closeQuery(info->q_prefix);
        info->q_prefix = NULL;
        info->flags.bit.have_prefix = 0;
    }

    if(info->q_name != NULL) {
        netmodel_closeQuery(info->q_name);
        info->q_name = NULL;
        info->flags.bit.intf_exist = 0;
    }

    info->q_prefix = netmodel_openQuery_getIPv6Prefixes((const char*) info->intf_path,
                                                        "dhcp6s-manager",
                                                        NULL,
                                                        netmodel_traverse_down,
                                                        interface_prefix_list_cb,
                                                        (void*) info);
    if(info->q_prefix == NULL) {
        SAH_TRACEZ_ERROR(ME, "Pool[%s] failed to open ipv6prefix query", info->pool_name);
    }

    info->q_name = netmodel_openQuery_getAddrs((const char*) info->intf_path,
                                               "dhcp6s-manager",
                                               "ipv6 && link",
                                               netmodel_traverse_down,
                                               interface_name_cb,
                                               (void*) info);
    if(info->q_name == NULL) {
        SAH_TRACEZ_ERROR(ME, "Pool[%s] failed to open name query", info->pool_name);
    }
    return;
exit:
    free(intf_path);
    return;
}

/**
 * @brief
 * Subscribe to netmodel queries and initialise the interface path in the private data of the model.
 *
 * @param obj The amxd pool object.
 */
void server_info_enable(amxd_object_t* obj) {
    server_info_t* info;
    SAH_TRACEZ_INFO(ME, "Enabling NetModel query subscription.");

    when_null_trace(obj, exit, ERROR, "Cannot get the pool object.");
    when_null_trace(obj->priv, exit, ERROR, "Cannot get private server info data from pool object.");

    info = (server_info_t*) obj->priv;

    amxd_object_for_all(info->obj, ".Client.*", dm_remove_client, info);
    amxc_var_clean((info->last_client_htable));

    server_info_open_queries(obj);
    sync_pool_status(obj);
exit:
    return;
}

/**
 * @brief
 * Allocate memory for private server info data to a pool object.
 *
 * @param obj The pool object.
 */
void server_info_create(amxd_object_t* obj) {
    server_info_t* info = NULL;
    amxc_string_t buf;

    info = (server_info_t*) calloc(1, sizeof(server_info_t));
    when_null_trace(info, exit, ERROR, "Cannot allocate memory for private data info.");
    obj->priv = info;
    info->obj = obj;
    info->flags.all_flags = 0;
    info->pool_name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    info->q_name = NULL;
    info->q_prefix = NULL;

    amxc_string_init(&buf, 0);
    amxc_string_setf(&buf, "cpe-dhcpv6s-%s", info->pool_name);
    info->fw_alias = amxc_string_take_buffer(&buf);
    amxc_string_clean(&buf);
exit:
    return;
}

void option_23_info_update(amxd_object_t* pool, amxd_object_t* dns) {
    const char* interface = NULL;
    const char* modes = NULL;
    option_23_info_t* option_info = NULL;

    when_null_trace(pool, exit, ERROR, "No pool object given to creat option 23");
    when_null_trace(dns, exit, INFO, "No dsn object created");

    interface = GET_CHAR(amxd_object_get_param_value(pool, "Interface"), NULL);
    modes = GET_CHAR(amxd_object_get_param_value(dns, "DNSMode"), NULL);

    if(dns->priv == NULL) {
        option_info = (option_23_info_t*) calloc(1, sizeof(option_23_info_t));
        when_null_trace(option_info, exit, ERROR, "Failed to allocate memory for private data");
        option_info->q_dnsserver = NULL;
        option_info->obj = dns;
        option_info->dynamic_addr = NULL;
        option_info->static_addr = NULL;
        dns->priv = option_info;
    } else {
        option_info = (option_23_info_t*) dns->priv;
        option_info->flags.all_flags = 0;
    }

    option_info->expected_flags.all_flags = 0;
    option_info->flags.all_flags = 0;

    if(strstr(modes, "Static") != NULL) {
        option_info->expected_flags.bit.static_ok = 1;
        option_info->flags.bit.static_ok = 1;
        free(option_info->static_addr);
        option_info->static_addr = amxd_object_get_value(cstring_t, dns, "StaticServers", NULL);
    } else {
        option_info->expected_flags.bit.static_ok = 0;
    }

    if(option_info->expected_flags.bit.static_ok != option_info->flags.bit.static_ok) {
        free(option_info->static_addr);
        option_info->static_addr = NULL;
    }

    if(strstr(modes, "GUA") != NULL) {
        option_info->expected_flags.bit.gua_ok = 1;
    }

    if(strstr(modes, "LLA") != NULL) {
        option_info->expected_flags.bit.lla_ok = 1;
    }

    if(option_info->expected_flags.bit.gua_ok || option_info->expected_flags.bit.lla_ok) {
        amxc_string_t query_name;

        amxc_string_init(&query_name, 0);

        if(option_info->q_dnsserver != NULL) {
            netmodel_closeQuery(option_info->q_dnsserver);
            option_info->q_dnsserver = NULL;
        }

        amxc_string_setf(&query_name, "%s-option_23", amxd_object_get_name(pool, AMXD_OBJECT_NAMED));

        option_info->q_dnsserver = netmodel_openQuery_getAddrs(interface,
                                                               amxc_string_get(&query_name, 0),
                                                               "ipv6",
                                                               netmodel_traverse_down,
                                                               dhcpv6_dns_change_cb,
                                                               option_info);
        if(option_info->q_dnsserver == NULL) {
            SAH_TRACEZ_ERROR(ME, "Unable to open LLA or GUA netmodel query");
        }
        amxc_string_clean(&query_name);
    }

    dm_mngr_update_option_23(pool, dns);
exit:
    return;
}