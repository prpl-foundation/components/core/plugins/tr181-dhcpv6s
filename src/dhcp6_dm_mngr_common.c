/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include "dhcp6_dm_mngr.h"
#include "dm_dhcp6.h"
#include "firewall_utility.h"

/**
 * @brief
 * Set the alias parameter to be compatible with uci alias.
 *
 * @param params The htable variant with an Alias filed to be changed.
 * @return amxd_status_ok if the Alias filed is correctly changed.
 */
amxd_status_t set_alias(amxc_var_t* params) {
    int status = -1;
    const char* alias = NULL;
    amxc_string_t alias_string;

    amxc_string_init(&alias_string, 0);

    when_null_trace(params, exit, ERROR, "Cannot get parameter htable.");

    alias = GET_CHAR(params, "Alias");
    when_null_trace(alias, exit, ERROR, "Unable to get Alias from parameter.");

    amxc_string_set(&alias_string, alias);
    amxc_string_replace(&alias_string, "-", "_", UINT32_MAX);

    amxc_var_add_key(cstring_t, params, "dhcpv6s181alias", alias);
    status = amxc_var_push(amxc_string_t, GET_ARG(params, "Alias"), &alias_string);
    when_failed_trace(status, exit, ERROR, "Unable to push alias in parameters.");

    status = amxd_status_ok;
exit:
    amxc_string_clean(&alias_string);
    return status;
}

/**
 * @brief
 * Fill the data variable with the dhcpv6 pool server's data to be usable by the controller.
 *
 * @param data Data to be filled for the controller.
 * @param info Pool private data.
 * @return amxd_status_t 0 if all the data where successfully added.
 */
static amxd_status_t fill_data_for_controller(amxc_var_t* data, server_info_t* info) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* params = NULL;
    amxc_var_t* opt_var = NULL;
    const char* prefix = GET_CHAR(amxo_parser_get_config(dhcp6_get_parser(), "prefix_"), NULL);
    when_null_trace(prefix, exit, ERROR, "Cannot get plugin prefix");

    when_null_trace(data, exit, ERROR, "No data to be filled for the controller.");
    when_null_trace(info, exit, ERROR, "No private information data given for the pool.");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, data, PARAMS, NULL);
    when_null_trace(params, exit, ERROR, "Unable to add parameters key to data.");
    amxd_object_get_params(info->obj, params, amxd_dm_access_protected);
    status = set_alias(params);
    when_failed_trace(status, exit, ERROR, "Unable to set alias.");
    amxc_var_add_key(cstring_t, params, "Interface", info->intf_path);
    amxc_var_add_key(cstring_t, params, "InterfaceName", info->intf_name);

    if(GET_BOOL(params, "IANAEnable")) {
        amxc_string_t path;
        char* ipv6_str = NULL;

        amxc_string_init(&path, 0);
        amxc_string_setf(&path, "%sIANAConfig.LeaseTime", prefix);
        amxc_var_add_key(int32_t, params, "LeaseTime", amxd_object_get_value(int32_t, info->obj, amxc_string_get(&path, 0), NULL));

        amxc_string_replace(&path, "LeaseTime", "MinAddress", UINT32_MAX);
        ipv6_str = amxd_object_get_value(cstring_t, info->obj, amxc_string_get(&path, 0), NULL);
        amxc_var_add_key(cstring_t, params, "MinAddress", ipv6_str);
        free(ipv6_str);

        amxc_string_replace(&path, "MinAddress", "MaxAddress", UINT32_MAX);
        ipv6_str = amxd_object_get_value(cstring_t, info->obj, amxc_string_get(&path, 0), NULL);
        amxc_var_add_key(cstring_t, params, "MaxAddress", ipv6_str);
        free(ipv6_str);

        amxc_string_clean(&path);
    }

    opt_var = amxc_var_add_key(amxc_llist_t, params, "Option", NULL);
    amxd_object_for_each(instance, it, amxd_object_findf(info->obj, ".Option.")) {
        amxd_object_t* option = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t* option_list = amxc_var_add(amxc_htable_t, opt_var, NULL);
        const char* passthrough_value = NULL;
        amxd_object_get_params(option, option_list, amxd_dm_access_protected);
        passthrough_value = GET_CHAR(option_list, "PassthroughClientValue");
        if(!str_empty(passthrough_value)) {
            amxc_var_set(cstring_t, GET_ARG(option_list, "Value"), passthrough_value);
        }
    }

exit:
    return status;
}

/**
 * @brief
 * Set the pool status in the data model and controller config.
 *
 * @param pool The pool amxd object with the status to be update.
 * @return amxd_status_ok if every status have been updated.
 */
amxd_status_t sync_pool_status(amxd_object_t* pool) {
    amxd_status_t status = amxd_status_unknown_error;
    server_info_t* info = NULL;
    amxd_trans_t trans;
    char* controller = NULL;
    bool enable = false;
    amxc_var_t ret;
    amxc_var_t data;

    amxc_var_init(&ret);
    amxc_var_init(&data);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, pool);

    when_null_trace(pool, exit, ERROR, "Cannot get pool object.");
    info = (server_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "Cannot get private server info from pool.");

    status = fill_data_for_controller(&data, info);
    when_failed_trace(status, exit, ERROR, "Cannot fill data for controller.");

    controller = amxd_object_get_value(cstring_t, info->obj, "ConfigController", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot gets controller from pool.");
    if((info->flags.bit.enable) && !(info->flags.bit.firewall_up)) {
        firewall_open_dhcp_port(pool);
        info->flags.bit.firewall_up = 1;
    }

    enable = info->flags.bit.enable && info->flags.bit.intf_exist && info->flags.bit.have_prefix;
    status = amxm_execute_function(controller,
                                   MOD_DHCPV6S_UCI,
                                   enable ? "enable-pool" : "disable-pool",
                                   &data,
                                   &ret);
    info->flags.bit.serv_ok = status == amxd_status_ok;

    if(!(info->flags.bit.enable) && (info->flags.bit.firewall_up)) {
        firewall_close_dhcp_port(pool);
        info->flags.bit.firewall_up = 0;
    }

    if(enable && info->flags.bit.serv_ok && info->flags.bit.firewall_up) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
    }
    if(!info->flags.bit.enable) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    }
    status = amxd_trans_apply(&trans, dhcp6_get_dm());
    when_failed_trace(status, exit, ERROR, "Cannot update Status of pool.");
exit:
    free(controller);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    amxd_trans_clean(&trans);
    return status;
}