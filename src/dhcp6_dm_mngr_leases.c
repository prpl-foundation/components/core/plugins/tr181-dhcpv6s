/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <limits.h>

#include <amxc/amxc.h>
#include <v4v6option.h>

#include "dhcp6_dm_mngr.h"
#include "dm_dhcp6.h"
#include "dhcp6_server_info.h"

#define GET_FIRST_STRING_LIST(list) \
    amxc_string_get(amxc_string_from_llist_it(amxc_llist_get_first(list)), 0)

/**
 * @brief
 * Add a lease to the client data model.
 *
 * @param lease The lease to add.
 * @param client The client to add the lease to.
 */
static void dm_add_lease(amxc_var_t* lease,
                         amxd_trans_t* trans) {
    amxc_string_t prefix_str;
    const char* ipv6 = NULL;
    bool prefix = false;
    uint32_t prefix_len = 0;
    const amxc_ts_t* validt = NULL;
    const amxc_ts_t* preferredt = NULL;

    amxc_string_init(&prefix_str, 0);

    when_null_trace(lease, exit, ERROR, "no lease given to add.");

    ipv6 = GETP_CHAR(lease, "address");
    prefix_len = GET_UINT32(lease, "prefix-length");
    validt = amxc_var_constcast(amxc_ts_t, GET_ARG(lease, "valid-lifetime"));
    preferredt = amxc_var_constcast(amxc_ts_t, GET_ARG(lease, "preferred-lifetime"));

    prefix = (prefix_len > 0 && prefix_len < 128);

    if(prefix) {
        amxc_string_setf(&prefix_str, "%s/%d", ipv6, prefix_len);
        ipv6 = amxc_string_get(&prefix_str, 0);
    } else {
        ipv6 = GETP_CHAR(lease, "address");
    }

    amxd_trans_select_pathf(trans, prefix ? ".IPv6Prefix." : ".IPv6Address.");
    amxd_trans_add_inst(trans, 0, NULL);
    amxd_trans_set_value(cstring_t, trans, prefix ? "Prefix" : "IPAddress", ipv6);

    amxd_trans_set_value(amxc_ts_t, trans, "PreferredLifetime", (amxc_ts_t*) preferredt);
    amxd_trans_set_value(amxc_ts_t, trans, "ValidLifetime", (amxc_ts_t*) validt);
    amxd_trans_select_pathf(trans, ".^.^"); // return back to previous path for next user of "trans"

exit:
    amxc_string_clean(&prefix_str);
    return;
}

static void add_option_instances(amxc_var_t* options, amxd_trans_t* trans) {
    when_null(options, exit);

    amxd_trans_select_pathf(trans, ".Option.");

    amxc_var_for_each(option, options) {
        const char* key = amxc_var_key(option);
        long int tmp = strtol(key, NULL, 10);
        uint16_t tag = 0;

        if((errno == ERANGE) ||
           (tmp < 0) ||
           (tmp > USHRT_MAX)) {
            SAH_TRACEZ_ERROR(ME, "Failed to convert '%s' to dhcpv6 option number", key);
            continue;
        }

        tag = (uint16_t) tmp;

        if(tag == 39) {
            amxc_var_t hex;
            amxc_var_t src;
            bool rv = false;

            amxc_var_init(&hex);
            amxc_var_init(&src);

            amxc_var_set_type(&src, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &src, "Flag", "0");
            amxc_var_add_key(cstring_t, &src, "Domains", GET_CHAR(option, NULL));
            rv = dhcpoption_v6_option_to_hex(tag, &hex, &src);
            if(!rv) {
                SAH_TRACEZ_ERROR(ME, "Failed to convert '%s' to hex", GET_CHAR(option, NULL));
                amxc_var_clean(&hex);
                amxc_var_clean(&src);
                break;
            }
            amxd_trans_add_inst(trans, 0, NULL);
            amxd_trans_set_value(uint16_t, trans, "Tag", tag);
            amxd_trans_set_value(cstring_t, trans, "Value", GET_CHAR(&hex, NULL));
            amxd_trans_select_pathf(trans, ".^");

            amxc_var_clean(&hex);
            amxc_var_clean(&src);
        }
    }

    amxd_trans_select_pathf(trans, ".^"); // return to previous path for next user of 'trans'
exit:
    return;
}

/**
 * @brief
 * Add a client to the pool data model.
 *
 * @param client The client object to add.
 * @param pool The pool to add the client to.
 */
static void dm_add_client(amxc_var_t* client, amxd_object_t* pool) {
    amxd_trans_t trans;
    amxc_var_t* leases = NULL;
    const char* duid = NULL;
    int retval = 0;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(client, exit, ERROR, "Cannot add null client to pool.");
    when_null_trace(pool, exit, ERROR, "Cannot add client to null pool.");

    duid = amxc_var_key(client);
    when_null_trace(duid, exit, ERROR, "No DUID given to the client by dhcpv6 server.");

    amxd_trans_select_object(&trans, pool);
    amxd_trans_select_pathf(&trans, ".Client.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "DUID", duid);

    add_option_instances(GET_ARG(client, "options"), &trans);

    leases = GET_ARG(client, "ipv6-addr");
    if(leases != NULL) {
        amxc_var_for_each(lease, leases) {
            dm_add_lease(lease, &trans);
        }
    } else {
        SAH_TRACE_INFO("No ipv6 address given.");
    }

    leases = GET_ARG(client, "ipv6-prefix");
    if(leases != NULL) {
        amxc_var_for_each(lease, leases) {
            dm_add_lease(lease, &trans);
        }
    } else {
        SAH_TRACE_INFO("No ipv6 prefix given.");
    }

    retval = amxd_trans_apply(&trans, dhcp6_get_dm());
    when_failed_trace(retval, exit, ERROR, "Unable to add client instance with duid %s and this return value %d.", duid, retval);

exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Checks the prefix content of the htable while deleting it from the htable
 *
 * @param pref Prefix variant
 * @param list The htable variant
 * @return true
 * @return false
 */
static bool check_prefix_entry_exist(amxc_var_t* pref, amxc_var_t* list) {
    amxc_var_t* pref_found = NULL;
    amxc_string_t prefix;

    amxc_string_init(&prefix, 0);

    when_null_trace(pref, exit, ERROR, "No prefix entry given.");
    when_null_trace(list, exit, ERROR, "No list given");


    amxc_string_setf(&prefix, "%s/%d", GET_CHAR(pref, "address"), GET_UINT32(pref, "prefix-length"));
    pref_found = amxc_var_get_key(list, amxc_string_get(&prefix, 0), AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(pref_found, exit, WARNING, "Prefix entry not found in list.");

    amxc_var_delete(&pref_found);
    amxc_string_clean(&prefix);
    return true;

exit:
    amxc_string_clean(&prefix);
    return false;
}

/**
 * @brief
 * Check that the ip entry is already in the htable of previous ip
 *
 * @param ip The Ip variant
 * @param list The htable variant
 * @return true
 * @return false
 */
static bool check_ip_entry_exist(amxc_var_t* ip, amxc_var_t* list) {
    amxc_var_t* ip_found = NULL;
    when_null_trace(ip, exit, ERROR, "No ip entry given.");
    when_null_trace(list, exit, ERROR, "No list given");

    ip_found = amxc_var_get_key(list, GET_CHAR(ip, "address"), AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(ip_found, exit, WARNING, "IP entry not found in list.");

    amxc_var_delete(&ip_found);
    return true;
exit:
    return false;
}

/**
 * @brief
 * Updates the found ip address of the client into the datamodel
 *
 * @param ip The ip entry to add
 * @param client_object The client object to update with the ip entry.
 */
static void dm_update_client_ip_entry(amxc_var_t* ip, amxd_object_t* client_object) {

    amxd_object_t* ip_object = NULL;
    amxd_trans_t trans;
    amxc_ts_t* validt = NULL;
    amxc_ts_t* preferredt = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(ip, exit, ERROR, "No ip entry given.");

    ip_object = amxd_object_findf(client_object,
                                  ".IPv6Address.[IPAddress=='%s'].",
                                  GET_CHAR(ip, "address"));

    SAH_TRACE_INFO("The IP to update is: %s", GET_CHAR(ip, "address"));

    when_null_trace(ip_object, exit, ERROR, "No ip entry could be found to update, error in the object fetching string.");

    amxd_trans_select_object(&trans, ip_object);

    validt = amxc_var_dyncast(amxc_ts_t, GET_ARG(ip, "valid-lifetime"));
    preferredt = amxc_var_dyncast(amxc_ts_t, GET_ARG(ip, "preferred-lifetime"));
    amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", preferredt);
    amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", validt);

    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot update ip entry.");
exit:
    free(validt);
    free(preferredt);
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Updates the prefix entry into the client datamodel
 *
 * @param pref The prefix to update the datamodel with
 * @param client_object The client instance that need the prefix to be updated
 */
static void dm_update_client_prefix_entry(amxc_var_t* pref, amxd_object_t* client_object) {
    amxd_object_t* pref_object = NULL;
    amxd_trans_t trans;
    amxc_ts_t* validt = NULL;
    amxc_ts_t* preferredt = NULL;
    amxc_string_t prefix;

    amxc_string_init(&prefix, 0);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pref, exit, ERROR, "No prefix entry given.");

    amxc_string_setf(&prefix, "%s/%d", GET_CHAR(pref, "address"), GET_UINT32(pref, "prefix-length"));
    pref_object = amxd_object_findf(client_object,
                                    ".IPv6Prefix.[Prefix=='%s'].",
                                    amxc_string_get(&prefix, 0));

    when_null_trace(pref_object, exit, ERROR, "No prefix entry could be found to update, error in the object fetching string.");

    amxd_trans_select_object(&trans, pref_object);

    validt = amxc_var_dyncast(amxc_ts_t, GET_ARG(pref, "valid-lifetime"));
    preferredt = amxc_var_dyncast(amxc_ts_t, GET_ARG(pref, "preferred-lifetime"));
    amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", preferredt);
    amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", validt);

    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot update prefix entry.");

exit:
    free(validt);
    free(preferredt);
    amxc_string_clean(&prefix);
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Add the ip entry to the client datamodel
 *
 * @param ip The ip entry to add
 * @param client_object The targeted client into the datamodel
 */
static void dm_add_client_ip_entry(amxc_var_t* ip, amxd_object_t* client_object) {

    amxd_trans_t trans;
    amxc_ts_t* validt = NULL;
    amxc_ts_t* preferredt = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(ip, exit, ERROR, "No ip entry given.");
    when_null_trace(client_object, exit, ERROR, "No client object given.");

    amxd_trans_select_object(&trans, client_object);
    amxd_trans_select_pathf(&trans, ".IPv6Address.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "IPAddress", GET_CHAR(ip, "address"));
    validt = amxc_var_dyncast(amxc_ts_t, GET_ARG(ip, "valid-lifetime"));
    preferredt = amxc_var_dyncast(amxc_ts_t, GET_ARG(ip, "preferred-lifetime"));
    amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", preferredt);
    amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", validt);

    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot add ip entry.");
exit:
    free(validt);
    free(preferredt);
    amxd_trans_clean(&trans);
    return;
}

static void dm_add_client_prefix_entry(amxc_var_t* pref, amxd_object_t* client_object) {
    amxd_trans_t trans;
    amxc_ts_t* validt = NULL;
    amxc_ts_t* preferredt = NULL;
    amxc_string_t prefix_str;

    amxc_string_init(&prefix_str, 0);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pref, exit, ERROR, "No prefix entry given.");
    when_null_trace(client_object, exit, ERROR, "No client object given.");

    amxd_trans_select_object(&trans, client_object);
    amxd_trans_select_pathf(&trans, ".IPv6Prefix.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxc_string_setf(&prefix_str, "%s/%d", GET_CHAR(pref, "address"), GET_UINT32(pref, "prefix-length"));
    amxd_trans_set_value(cstring_t, &trans, "Prefix", amxc_string_get(&prefix_str, 0));
    validt = amxc_var_dyncast(amxc_ts_t, GET_ARG(pref, "valid-lifetime"));
    preferredt = amxc_var_dyncast(amxc_ts_t, GET_ARG(pref, "preferred-lifetime"));
    amxd_trans_set_value(amxc_ts_t, &trans, "PreferredLifetime", preferredt);
    amxd_trans_set_value(amxc_ts_t, &trans, "ValidLifetime", validt);

    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot add prefix entry.");

exit:
    free(validt);
    free(preferredt);
    amxc_string_clean(&prefix_str);
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Removes an prefix entry of the client from the data model
 *
 * @param pref The prefix entry
 * @param client_object The client from the datamodel
 */
static void dm_remove_client_prefix_entry(amxc_var_t* pref,
                                          amxd_object_t* client_object) {
    amxd_object_t* prefix_object = NULL;
    const char* prefix_string = GET_CHAR(pref, NULL);
    when_str_empty_trace(prefix_string, exit, WARNING, "No prefix entry given.");
    prefix_object = amxd_object_findf(client_object,
                                      ".IPv6Prefix.[Prefix=='%s'].",
                                      prefix_string);

    amxd_object_emit_del_inst(prefix_object);
    amxd_object_delete(&prefix_object);
exit:
    return;
}

/**
 * @brief
 * Removes an ip entry of the client from the data model
 *
 * @param ip The ip entry
 * @param client_object The client from the datamodel
 */
static void dm_remove_client_ip_entry(amxc_var_t* ip,
                                      amxd_object_t* client_object) {
    amxd_object_t* ip_object = NULL;
    const char* ip_string = GET_CHAR(ip, NULL);
    when_str_empty_trace(ip_string, exit, WARNING, "No ip entry given.");
    ip_object = amxd_object_findf(client_object,
                                  ".IPv6Address.[IPAddress=='%s'].",
                                  ip_string);

    amxd_object_emit_del_inst(ip_object);
    amxd_object_delete(&ip_object);
exit:
    return;
}

/**
 * @brief
 * Updates the client to the DHCPv6s datamodel
 *
 * @param client the client to add.
 * @param pool The pool that will need its client to be updated
 */
static void dm_update_client(amxc_var_t* client,
                             amxd_object_t* pool,
                             amxc_var_t* last_client) {
    amxd_object_t* client_object = NULL;
    amxc_var_t* last_ip_list = NULL;
    amxc_var_t* last_prefix_list = NULL;
    const char* duid = NULL;

    when_null_trace(client, exit, ERROR, "No service given.");
    when_null_trace(pool, exit, ERROR, "No pool object given.");

    duid = amxc_var_key(client);

    client_object = amxd_object_findf(pool, ".Client.[DUID=='%s'].", duid);
    when_null_trace(client_object, exit, ERROR, "No client could be found in the pool.");

    last_ip_list = GET_ARG(last_client, "ipv6-addr");

    last_prefix_list = GET_ARG(last_client, "ipv6-prefix");

    amxc_var_for_each(ip, GET_ARG(client, "ipv6-addr")) {
        if(check_ip_entry_exist(ip, last_ip_list)) {
            dm_update_client_ip_entry(ip, client_object);
        } else {
            dm_add_client_ip_entry(ip, client_object);
        }
    }

    amxc_var_for_each(pref, GET_ARG(client, "ipv6-prefix")) {
        if(check_prefix_entry_exist(pref, last_prefix_list)) {
            dm_update_client_prefix_entry(pref, client_object);
        } else {
            dm_add_client_prefix_entry(pref, client_object);
        }
    }

    amxc_var_for_each(ip, last_ip_list) {
        dm_remove_client_ip_entry(ip, client_object);
    }

    amxc_var_for_each(pref, last_prefix_list) {
        dm_remove_client_prefix_entry(pref, client_object);
    }

exit:
    return;
}

/**
 * @brief
 * Check if the client is in the list of client and return it if present.
 *
 * @param client The client to be found.
 * @param htable The htable of client to check.
 * @return The client removed from the list.
 */
static amxc_var_t* check_client_exist(amxc_var_t* client, amxc_var_t* htable) {
    amxc_var_t* client_found = NULL;
    const char* duid = NULL;

    when_null_trace(client, exit, ERROR, "No client given.");
    when_null_trace(htable, exit, ERROR, "No htable given.");
    duid = amxc_var_key(client);
    client_found = amxc_var_get_key(htable, duid, AMXC_VAR_FLAG_DEFAULT);

    when_null_trace(client_found, exit, WARNING, "Client not found in list.");
    amxc_var_take_it(client_found);
exit:
    return client_found;
}

/**
 * @brief
 * Add a client to the client htbale
 *
 * @param htbale The htable to add the services to
 * @param client The client to add
 */
static void add_client_htable(amxc_var_t* htable, amxc_var_t* client) {
    amxc_var_t* client_param = NULL;
    amxc_var_t* ip_entries = NULL;
    amxc_var_t* prefix_entries = NULL;
    const char* duid = NULL;

    when_null_trace(htable, exit, ERROR, "No htable given.");
    when_null_trace(client, exit, ERROR, "No client given.");

    duid = amxc_var_key(client);

    client_param = amxc_var_add_key(amxc_htable_t, htable, duid, NULL);
    when_null_trace(client_param, exit, WARNING, "Could not create a client param key field.");

    ip_entries = amxc_var_add_key(amxc_htable_t, client_param, "ipv6-addr", NULL);
    prefix_entries = amxc_var_add_key(amxc_htable_t, client_param, "ipv6-prefix", NULL);


    amxc_var_for_each(ip, GET_ARG(client, "ipv6-addr")) {
        amxc_var_add_key(cstring_t, ip_entries, GET_CHAR(ip, "address"), GET_CHAR(ip, "address"));
    }

    amxc_var_for_each(pref, GET_ARG(client, "ipv6-prefix")) {
        amxc_string_t prefix;
        amxc_string_init(&prefix, 0);
        amxc_string_setf(&prefix, "%s/%d", GET_CHAR(pref, "address"), GET_UINT32(pref, "prefix-length"));
        amxc_var_add_key(cstring_t, prefix_entries, amxc_string_get(&prefix, 0), GET_CHAR(pref, "address"));
        amxc_string_clean(&prefix);
    }

exit:
    return;
}

/**
 * @brief
 * Remove a client from the dhcpv6s data model.
 *
 * @param client client to delete.
 * @param priv dhcpv6s private data.
 * @return int 0 always.
 */
int dm_remove_client(UNUSED amxd_object_t* templ,
                     amxd_object_t* client,
                     UNUSED void* priv) {
    when_null_trace(client, exit, WARNING, "Client to suppress is null.");
    amxd_object_emit_del_inst(client);
    amxd_object_delete(&client);
exit:
    return 0;
}

/**
 * @brief
 * Removes from the datamodel a whole client depending on the client reference
 *
 * @param client The client reference to delete in the datamodel
 * @param pool the pool from the datamodel where to delete the client instance
 * @return int
 */
static int dm_remove_client_with_variant(amxc_var_t* client,
                                         amxd_object_t* pool) {
    amxd_object_t* client_object = NULL;
    const char* duid = NULL;

    when_null_trace(client, exit, WARNING, "Client to suppress is null.");
    when_null_trace(pool, exit, ERROR, "Pool object is not given");

    duid = amxc_var_key(client);

    client_object = amxd_object_findf(pool, ".Client.[DUID=='%s'].", duid);

    when_null_trace(client_object, exit, ERROR, "No client entry could be found to remove, error in the object fetching string.");

    amxd_object_emit_del_inst(client_object);
    amxd_object_delete(&client_object);

exit:
    return 0;
}

/**
 * @brief
 * Add leases to the correct pool server. Remove all lease before.
 *
 * @param function_name Name of the function called.
 * @param args Leases to add.
 * @param ret The return variant.
 */
int dm_mngr_set_leases(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* pools = amxd_dm_findf(dhcp6_get_dm(), "DHCPv6Server.Pool.");
    amxc_var_t client_htable;

    when_null_trace(args, exit, ERROR, "Unable to get the data from call.");

    amxd_object_for_each(instance, it, pools) {
        amxd_object_t* pool = amxc_container_of(it, amxd_object_t, it);
        server_info_t* info = (server_info_t*) pool->priv;

        amxc_var_init(&client_htable);
        amxc_var_set_type(&client_htable, AMXC_VAR_ID_HTABLE);

        if((info != NULL) && (info->intf_name != NULL) && (strncmp(info->intf_name, amxc_var_key(amxc_var_get_first(args)), strlen(info->intf_name)) == 0)) {

            if(!(info->flags.bit.enable &&
                 info->flags.bit.intf_exist &&
                 info->flags.bit.have_prefix &&
                 info->flags.bit.serv_ok &&
                 info->flags.bit.firewall_up)) {
                status = amxd_status_ok;
                goto exit;
            }

            amxc_var_for_each(client, amxc_var_get_first(args)) {
                amxc_var_t* old_client = check_client_exist(client, (info->last_client_htable));
                if(old_client != NULL) {
                    dm_update_client(client, info->obj, old_client);
                    amxc_var_delete(&old_client);
                } else {
                    dm_add_client(client, info->obj);
                }
                add_client_htable(&client_htable, client);
            }

            amxc_var_for_each(client, (info->last_client_htable)) {
                dm_remove_client_with_variant(client, pool);
            }

            if(info->last_client_htable != NULL) {
                amxc_var_delete(&(info->last_client_htable));
            }
            amxc_var_new(&(info->last_client_htable));
            amxc_var_move((info->last_client_htable), &client_htable);
        }
        amxc_var_clean(&client_htable);
    }
    status = amxd_status_ok;
    return status;
exit:
    amxc_var_clean(&client_htable);
    return status;
}