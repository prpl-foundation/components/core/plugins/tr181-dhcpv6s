/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2025 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include "dhcp6_option_info.h"
#include "dm_dhcp6.h"
#include "dhcp6_priv.h"

static void received_option_value_cb(UNUSED const char* signame,
                                     const amxc_var_t* data,
                                     void* priv) {
    amxd_object_t* option = priv;
    amxc_var_t* params = GET_ARG(data, "parameters");

    when_null_trace(params, exit, ERROR, "Cannot get parameters from data callback");
    when_null_trace(option, exit, ERROR, "Cannot get option object.");

    dm_mngr_update_passthrough(option, GETP_CHAR(params, "Value.to"));
exit:
    return;
}

static void received_option_cb(UNUSED const char* signame,
                               const amxc_var_t* data,
                               void* priv) {
    amxd_object_t* option = priv;
    option_info_t* option_info = NULL;
    amxb_bus_ctx_t* bus = NULL;
    const char* option_path = NULL;
    amxc_var_t* params = NULL;
    const char* notification = NULL;
    const char* path = NULL;
    amxc_var_t option_value;
    amxc_string_t option_path_str;

    amxc_var_init(&option_value);
    amxc_string_init(&option_path_str, 0);

    when_null_trace(data, exit, ERROR, "Callback data is NULL");
    when_null_trace(option, exit, ERROR, "Cannot get option private data");

    option_info = option->priv;
    when_null_trace(option_info, exit, ERROR, "Cannot get option info");

    notification = GET_CHAR(data, "notification");
    when_null_trace(notification, exit, ERROR, "Cannot get notification type from data callback");

    path = GET_CHAR(data, "path");
    when_null_trace(path, exit, ERROR, "Cannot get path from data callback");

    params = GET_ARG(data, "parameters");
    when_null_trace(params, exit, ERROR, "Cannot get parameters from data callback");

    amxc_string_setf(&option_path_str, "%s%u.", path, GET_UINT32(data, "index"));
    option_path = amxc_string_get(&option_path_str, 0);

    if(strcmp(notification, "dm:instance-added") == 0) {
        SAH_TRACEZ_INFO(ME, "Found ReceivedOption %s with value %s", option_path, GET_CHAR(params, "Value"));
        dm_mngr_update_passthrough(option, GET_CHAR(params, "Value"));

        when_not_null_trace(option_info->option_value_subscription, exit, ERROR, "Already subscribed");
        bus = amxb_be_who_has(option_path);
        when_null_trace(bus, exit, ERROR, "No bus found for '%s'", option_path);
        amxb_subscription_new(&(option_info->option_value_subscription), bus, option_path, "contains('parameters.Value')", received_option_value_cb, option);
        when_null_trace(option_info->option_value_subscription, exit, ERROR, "Failed to subscribe to '%s", option_path);
    } else if(strcmp(notification, "dm:instance-removed") == 0) {
        SAH_TRACEZ_INFO(ME, "Removed ReceivedOption %s", option_path);
        dm_mngr_update_passthrough(option, "");

        when_null_trace(option_info->option_value_subscription, exit, ERROR, "Not subscribed");
        amxb_subscription_delete(&(option_info->option_value_subscription));
        option_info->option_value_subscription = NULL;
    }
exit:
    amxc_var_clean(&option_value);
    amxc_string_clean(&option_path_str);
    return;
}

void option_info_create(amxd_object_t* obj) {
    option_info_t* info = NULL;
    when_null_trace(obj, exit, ERROR, "No option object given.");
    info = (option_info_t*) calloc(1, sizeof(option_info_t));
    when_null_trace(info, exit, ERROR, "Cannot allocate memory for private data info.");
    obj->priv = info;
exit:
    return;
}

int option_info_clean(UNUSED amxd_object_t* templ,
                      amxd_object_t* option,
                      UNUSED void* priv) {
    option_info_t* info = NULL;

    when_null_trace(option, exit, ERROR, "No option object given.");
    info = (option_info_t*) option->priv;
    when_null_trace(info, exit, ERROR, "Cannot get the private option info from option object.");

    if(info->option_object_subscription != NULL) {
        amxb_subscription_delete(&(info->option_object_subscription));
        info->option_object_subscription = NULL;
    }

    if(info->option_value_subscription != NULL) {
        amxb_subscription_delete(&(info->option_value_subscription));
        info->option_value_subscription = NULL;
    }
    FREE(info->passthrough_path);
    FREE(option->priv);
exit:
    return 1;
}

void sync_passthrough_option(amxd_object_t* option) {
    amxc_string_t client_path;
    amxc_string_t expr;
    amxc_var_t client_opt_value;
    option_info_t* option_info = NULL;
    int retval = -1;
    const char* passtrough = NULL;
    uint16_t tag = 0;
    amxb_bus_ctx_t* bus = NULL;
    bool should_clean = false;

    amxc_string_init(&client_path, 0);
    amxc_string_init(&expr, 0);
    amxc_var_init(&client_opt_value);

    when_null_trace(option, exit, ERROR, "No option object given.");

    passtrough = GET_CHAR(amxd_object_get_param_value(option, "PassthroughClient"), NULL);
    tag = amxd_object_get_value(uint16_t, option, "Tag", NULL);

    option_info = option->priv;
    when_null_trace(option_info, exit, ERROR, "Cannot get the private option info from option object.");

    if(!str_empty(option_info->passthrough_path)) {
        if(str_empty(passtrough)) {
            should_clean = true;
        } else {
            should_clean = (strcmp(passtrough, option_info->passthrough_path) != 0);
        }
    }

    if(should_clean) {
        SAH_TRACEZ_INFO(ME, "Unsubscribing from Client '%s' option %d", option_info->passthrough_path, tag);
        if(option_info->option_object_subscription != NULL) {
            amxb_subscription_delete(&(option_info->option_object_subscription));
            option_info->option_object_subscription = NULL;
        }
        if(option_info->option_value_subscription != NULL) {
            amxb_subscription_delete(&(option_info->option_value_subscription));
            option_info->option_value_subscription = NULL;
        }
        FREE(option_info->passthrough_path);
        dm_mngr_update_passthrough(option, "");
    }

    if(str_empty(option_info->passthrough_path) && !str_empty(passtrough)) {
        SAH_TRACEZ_INFO(ME, "Subscribing to Client '%s' option %d", passtrough, tag);
        bus = amxb_be_who_has(passtrough);
        when_null_trace(bus, exit, ERROR, "No bus found for '%s'", passtrough);

        amxc_string_setf(&client_path, "%sReceivedOption.", passtrough);
        amxc_string_setf(&expr, "parameters.Tag == %u", tag);
        amxb_subscription_new(&(option_info->option_object_subscription), bus, amxc_string_get(&client_path, 0), amxc_string_get(&expr, 0), received_option_cb, option);
        when_null_trace(option_info->option_object_subscription, exit, ERROR, "Failed to subscribe to '%s", amxc_string_get(&client_path, 0));

        option_info->passthrough_path = strdup(passtrough);

        amxc_string_appendf(&client_path, "[Tag == '%u'].", tag);
        retval = amxb_get(bus, amxc_string_get(&client_path, 0), 0, &client_opt_value, 1);
        when_failed_trace(retval, exit, ERROR, "Unable to get '%s'", amxc_string_get(&client_path, 0));
        if(GETP_CHAR(&client_opt_value, "0.0.Value") != NULL) {
            when_not_null_trace(option_info->option_value_subscription, exit, ERROR, "Already subscribed to option 'Value' parameter");
            amxb_subscription_new(&(option_info->option_value_subscription), bus, amxc_string_get(&client_path, 0), "contains('parameters.Value')", received_option_value_cb, option);
            when_null_trace(option_info->option_value_subscription, exit, ERROR, "Failed to subscribe to '%s", amxc_string_get(&client_path, 0));
            dm_mngr_update_passthrough(option, GETP_CHAR(&client_opt_value, "0.0.Value"));
        }
    }
exit:
    amxc_var_clean(&client_opt_value);
    amxc_string_clean(&client_path);
    amxc_string_clean(&expr);
    return;
}