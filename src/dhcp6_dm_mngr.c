/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dhcp6_dm_mngr.h"
#include "dm_dhcp6.h"
#include "dhcp6_server_info.h"
#include "firewall_utility.h"

amxm_module_t* mod = NULL;

/**
 * @brief
 * Load a list of controller
 *
 * @param path The path where the module of the controller can be found.
 * @param controller_list_name The name of the controllers' list.
 * @return int 0 if loading successful.
 */
static int load_list_controller(const char* path, const char* controller_list_name) {
    int retval = 0;
    amxc_var_t lcontrollers;
    const amxc_var_t* controllers = NULL;
    amxm_shared_object_t* so = NULL;
    amxd_dm_t* dm = dhcp6_get_dm();
    amxd_object_t* dhcp6s = amxd_dm_findf(dm, "DHCPv6Server.");
    amxo_parser_t* parser = dhcp6_get_parser();

    when_null_trace(path, exit, ERROR, "No path given.");
    when_null_trace(controller_list_name, exit, ERROR, "No name for controller list given.");

    controllers = amxd_object_get_param_value(dhcp6s, controller_list_name);
    amxc_var_init(&lcontrollers);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        amxc_string_t mod_path;
        const char* name = GET_CHAR(controller, NULL);

        amxc_string_init(&mod_path, 0);
        amxc_string_setf(&mod_path, "%s%s.so", path, name);
        amxc_string_resolve_var(&mod_path, &parser->config);
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to load %s", amxc_string_get(&mod_path, 0));
        }
        amxc_string_clean(&mod_path);
        when_failed(retval, exit);
    }
exit:
    amxc_var_clean(&lcontrollers);
    return retval;
}

/**
 * @brief
 * Load all modules' controller.
 *
 * @return int 0 if loading successful.
 */
static int dm_mngr_load_controllers(void) {
    int retval = 1;

    retval = load_list_controller("${mod-path}/", "SupportedConfigControllers");
    when_failed_trace(retval, exit, ERROR, "Unable to load server controller.");
    retval = load_list_controller("${mod-path}/", "SupportedServerControllers");
    when_failed_trace(retval, exit, ERROR, "Unable to load server controller.");
    retval = load_list_controller("/usr/lib/amx/modules/", "SupportedFWControllers");
    when_failed_trace(retval, exit, ERROR, "Unable to load firewall controller.");
exit:
    return retval;
}

/**
 * @brief
 * Initialise DHCPv6 manager.
 *
 * @return int 0 when initialisation is successfull.
 */
int dhcp6s_dm_mngr_init(void) {
    int retval = 1;
    amxm_shared_object_t* so = amxm_get_so("self");
    when_null_trace(so, exit, ERROR, "Unable to get shared object.");
    when_failed(amxm_module_register(&mod, so, MOD_DM_MNGR), exit);
    amxm_module_add_function(mod, "set-leases", dm_mngr_set_leases);

    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to init netmodel");
    retval = dm_mngr_load_controllers();
    when_failed_trace(retval, exit, ERROR, "Unable to load controllers.");
exit:
    return retval;
}

/**
 * @brief
 * Clean DHCPv6 manager.
 *
 * @return int 0 when cleaningis successfull.
 */
int dhcp6s_dm_mngr_clean(void) {
    int retval = 1;
    amxd_object_t* pools = amxd_dm_findf(dhcp6_get_dm(), "DHCPv6Server.Pool.");

    when_null_trace(pools, exit, ERROR, "Unable to clean dhcpv6 server");
    amxd_object_for_each(instance, it, pools) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        server_info_t* info = NULL;
        when_null(inst, exit);
        info = (server_info_t*) inst->priv;
        server_info_clear(info);
    }
    amxm_module_deregister(&mod);
exit:
    return retval;
}