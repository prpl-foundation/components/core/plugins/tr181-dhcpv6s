/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp6_priv.h"
#include "dm_dhcp6.h"

#include <amxb/amxb_operators.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <v4v6option.h>

#undef ME
#define ME "dhcp6s"

static void add_pool_arguments(amxc_var_t* dest, amxd_object_t* obj) {
    amxd_object_t* pool_obj = amxd_object_findf(obj, ".^.^.^.^");
    const char* interface = NULL;

    interface = GET_CHAR(amxd_object_get_param_value(pool_obj, "Interface"), NULL);
    when_str_empty_trace(interface, exit, ERROR, "Pool's Interface parameter is empty");

    amxc_var_add_key(cstring_t, dest, "Interface", interface);
exit:
    return;
}

static void add_client_arguments(amxc_var_t* dest, amxd_object_t* obj) {
    amxd_object_t* client_obj = amxd_object_findf(obj, ".^.^");
    amxc_var_t* ip_addresses = NULL;
    uint32_t count = 0;
    bool active = amxd_object_get_value(bool, client_obj, "Active", NULL);

    ip_addresses = amxc_var_add_key(amxc_llist_t, dest, "IPAddresses", NULL);

    amxd_object_for_each(instance, it, amxd_object_findf(client_obj, ".IPv6Address.")) {
        amxd_object_t* ip_address_obj = amxc_container_of(it, amxd_object_t, it);
        const char* ip_address = GET_CHAR(amxd_object_get_param_value(ip_address_obj, "IPAddress"), NULL);
        amxc_var_add(cstring_t, ip_addresses, ip_address);
        count++;
    }

    amxc_var_cast(ip_addresses, AMXC_VAR_ID_CSV_STRING);

    amxc_var_add_key(bool, dest, "Enable", active && (count > 0));
}

static bool set_host(amxd_object_t* option_obj, amxc_var_t* extra_args) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int amxb_rv = -1;
    bool rv = false;
    char* alias = (char*) option_obj->priv;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "No bus found for 'DNS.'");

    if(extra_args == NULL) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    } else {
        amxc_var_move(&args, extra_args);
    }

    if(alias != NULL) {
        amxc_var_add_key(cstring_t, &args, "Alias", alias);
    }

    add_pool_arguments(&args, option_obj);
    add_client_arguments(&args, option_obj);
    amxc_var_add_key(bool, &args, "Exclusive", true);

    amxb_rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 5);
    when_false_trace(amxb_rv == AMXB_STATUS_OK, exit, ERROR, "Failed to exec DNS.SetHost: %d", amxb_rv);

    if(option_obj->priv == NULL) {
        option_obj->priv = amxc_var_take(cstring_t, GETI_ARG(&ret, 0)); // save the alias of the DNS.Host. instance
    }

    rv = true;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static bool remove_host(const char* alias) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int amxb_rv = -1;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "No bus found for 'DNS.'");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", alias);

    amxb_rv = amxb_call(bus, "DNS.", "RemoveHost", &args, &ret, 5);
    when_false_trace(amxb_rv == AMXB_STATUS_OK, exit, ERROR, "Failed to exec DNS.RemoveHost: %d", amxb_rv);

    rv = true;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static char* hostname_from_fqdn(char* fqdn) {
    char* end_of_hostname = strstr(fqdn, ".");
    if(end_of_hostname != NULL) {
        *end_of_hostname = '\0';
    }
    return fqdn;
}

void _dhcps_client_option_hostname_added(UNUSED const char* const event_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(dhcp6_get_dm(), data);
    amxd_object_t* option_obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_var_t* domain_name = NULL;
    amxc_var_t args;
    amxc_var_t fqdn;
    const char* value_hex = GET_CHAR(params, "Value");
    unsigned char* value_bin = NULL;
    char* hostname = NULL;
    uint32_t tag = GET_UINT32(params, "Tag");
    uint32_t length = 0;

    amxc_var_init(&args);
    amxc_var_init(&fqdn);

    when_false_trace(tag == 39, exit, WARNING, "Unexpected DHCP option %u, Client FQDN option is 39 (rfc4704)", tag);
    when_str_empty(value_hex, exit);

    value_bin = dhcpoption_option_convert2bin(value_hex, &length);
    when_null_trace(value_bin, exit, ERROR, "Failed to convert DHCP option(39) Client FQDN to binary: '%s'", value_hex);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Origin", "DHCPv6");

    dhcpoption_v6parse(&fqdn, 39, length, value_bin);

    domain_name = GET_ARG(&fqdn, "DomainName");
    when_str_empty_trace(GET_CHAR(domain_name, NULL), exit, ERROR, "Failed to convert DHCP option(39) Client FQDN: '%s'", value_hex);

    hostname = hostname_from_fqdn(amxc_var_take(cstring_t, domain_name));
    amxc_var_add_key(cstring_t, &args, "Name", hostname);

    set_host(option_obj, &args);

exit:
    amxc_var_clean(&fqdn);
    amxc_var_clean(&args);
    free(value_bin);
    free(hostname);
}

amxd_status_t _dhcps_client_option_destroy(amxd_object_t* object,
                                           UNUSED amxd_param_t* param,
                                           amxd_action_t reason,
                                           UNUSED const amxc_var_t* const args,
                                           UNUSED amxc_var_t* const retval,
                                           UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t tag = amxd_object_get_value(uint32_t, object, "Tag", NULL);

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_true_status(amxd_object_get_type(object) == amxd_object_template,
                     exit,
                     status = amxd_status_ok);

    if(tag == 39) {
        char* alias = (char*) object->priv;

        when_null_status(alias, exit, status = amxd_status_ok);

        if(*alias != '\0') {
            remove_host(alias);
        }

        FREE(object->priv);
    }

    status = amxd_status_ok;
exit:
    return status;
}

void _dhcps_client_option_hostname_ipaddress_changed(UNUSED const char* const event_name,
                                                     const amxc_var_t* const data,
                                                     UNUSED void* const priv) {
    amxd_object_t* ipaddress_obj = amxd_dm_signal_get_object(dhcp6_get_dm(), data);
    amxd_object_t* option_obj = amxd_object_findf(ipaddress_obj, ".^.Option.[Tag == 39].");

    when_null(option_obj, exit);

    set_host(option_obj, NULL);

exit:
    return;
}