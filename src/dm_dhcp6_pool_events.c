/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "dm_dhcp6.h"
#include "dhcp6_dm_mngr.h"

/**
 * @brief
 * Event handler function when there is a modification of a pool. Except for the interface which is handled differently.
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additionnal arguments, not used.
 */
void _dhcpv6s_pool_changed(UNUSED const char* const event_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    bool enable = false;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_dm_t* dm = NULL;
    amxd_object_t* obj = NULL;

    SAH_TRACEZ_INFO(ME, "In pool_changed handler event function.");

    dm = dhcp6_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv6 data model object.");
    obj = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(obj, exit, ERROR, "Unable to get the modified pool object.");
    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    status = enable ? enable_pool(obj) : disable_pool(obj);
    when_failed_trace(status, exit, ERROR, "Unable to enable or disable pool.");
exit:
    return;
}

/**
 * @brief
 * Event handler function when the Interface field of the pool change.
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additionnal arguments, not used.
 */
void _dhcpv6s_interface_changed(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    bool enable = false;
    amxd_dm_t* dm = NULL;
    amxd_object_t* obj = NULL;

    SAH_TRACEZ_INFO(ME, "In interface_changed event handler function.");

    dm = dhcp6_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv6 data model.");
    obj = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(obj, exit, ERROR, "Cannot get pool object.");

    status = disable_pool(obj);
    when_failed_trace(status, exit, ERROR, "Unable to disable pool.");
    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    status = enable ? enable_pool(obj) : disable_pool(obj);
    when_failed_trace(status, exit, ERROR, "Unable to enable or disable pool.");
exit:
    return;
}

/**
 * @brief
 * Handler event when a new pool is added to the DHCPv6.Server data model.
 *
 * @param event_name Name of the event send to the object, not used.
 * @param event_data data from the called event.
 * @param priv Additionnal arguments, not used.
 */
void _dhcpv6s_pool_added(UNUSED const char* const event_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    bool enable = false;
    amxd_dm_t* dm = NULL;
    amxd_object_t* templ = NULL;
    amxd_object_t* obj = NULL;
    const char* name = NULL;

    SAH_TRACEZ_INFO(ME, "In pool_added event handler function.");

    name = GET_CHAR(event_data, "name");
    when_null_trace(name, exit, ERROR, "Cannot get object name.");

    dm = dhcp6_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv6 data model.");
    templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(templ, exit, ERROR, "Cannot get template for pool object.");
    obj = amxd_object_get_instance(templ, name, 0);
    when_null_trace(obj, exit, ERROR, "Cannot get pool object.");

    enable = amxd_object_get_value(bool, obj, "Enable", NULL);
    status = enable ? enable_pool(obj) : disable_pool(obj);
    when_failed_trace(status, exit, ERROR, "Unable to enable or disable pool.");
exit:
    return;
}

// Debugging - print events - activate in odl
void _print_event(UNUSED const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    amxc_var_log(data);
}

void _dhcpv6s_option_add_or_remove(const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* option_templ = NULL;
    amxd_object_t* option = NULL;
    amxd_dm_t* dm = NULL;
    server_info_t* info = NULL;
    bool enabled = false;

    dm = dhcp6_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv6 data model.");
    option_templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(option_templ, exit, ERROR, "Cannot get DHCPv6 pool option object.");

    enabled = GETP_BOOL(event_data, "parameters.Enable");

    pool = amxd_object_findf(option_templ, "^");
    when_null_trace(pool, exit, ERROR, "Can't get DHCPv6 pool object");

    info = (server_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "DHCPv6 pool private data is NULL");

    option = amxd_object_get_instance(option_templ, NULL, GET_UINT32(event_data, "index"));

    if(strcmp(event_name, "dm:instance-added") == 0) {
        option_info_create(option);
        sync_passthrough_option(option);
    } else if(strcmp(event_name, "dm:instance-removed") == 0) {
        option_info_clean(NULL, option, NULL);
    }

    if((info->flags.bit.enable == 1) && enabled) {
        when_failed_trace(sync_pool_status(pool), exit, ERROR, "Failed to sync DHCPv6 pool status");
    }
exit:
    return;
}

void _dhcpv6s_option_changed(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    amxd_object_t* option = NULL;
    amxd_dm_t* dm = NULL;
    server_info_t* info = NULL;

    dm = dhcp6_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv6 data model.");
    option = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(option, exit, ERROR, "Cannot get DHCPv6 pool option object.");

    pool = amxd_object_findf(option, "^.^");
    when_null_trace(pool, exit, ERROR, "Can't get DHCPv6 pool object");

    info = (server_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "DHCPv6 pool private data is NULL");

    sync_passthrough_option(option);

    if(info->flags.bit.enable == 1) {
        when_failed_trace(sync_pool_status(pool), exit, ERROR, "Failed to update DHCPv6 pool status");
    }

exit:
    return;
}

void _dhcpv6s_ia_changed(UNUSED const char* const event_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    amxd_object_t* ia = NULL;
    amxd_object_t* pool = NULL;
    server_info_t* info = NULL;

    ia = amxd_dm_signal_get_object(dhcp6_get_dm(), event_data);
    when_null_trace(ia, exit, ERROR, "Cannot get DNS DHCPv6 pool object.");

    pool = amxd_object_get_parent(ia);
    when_null_trace(pool, exit, ERROR, "Cannot get pool object");
    when_null_trace(pool->priv, exit, ERROR, "Cannot get pool object private data");

    info = (server_info_t*) pool->priv;

    if(info->flags.bit.enable) {
        when_failed_trace(sync_pool_status(pool), exit, ERROR, "Unable to sync pool.");
    }
exit:
    return;
}

void _dhcpv6s_pool_option_23_changed(UNUSED const char* const event_name,
                                     const amxc_var_t* const event_data,
                                     UNUSED void* const priv) {
    amxd_object_t* dns = NULL;
    amxd_object_t* pool = NULL;

    dns = amxd_dm_signal_get_object(dhcp6_get_dm(), event_data);
    when_null_trace(dns, exit, ERROR, "Cannot get DNS DHCPv6 pool object.");

    pool = amxd_object_get_parent(dns);

    option_23_info_update(pool, dns);
exit:
    return;
}

void _dhcpv6s_pool_option_23_interface(UNUSED const char* const event_name,
                                       const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    amxd_object_t* dns = NULL;
    amxd_object_t* pool = NULL;

    pool = amxd_dm_signal_get_object(dhcp6_get_dm(), event_data);
    when_null_trace(pool, exit, ERROR, "Cannot get DHCPv6 pool object.");

    dns = amxd_object_findf(pool, ".%sDNS.", GET_CHAR(amxo_parser_get_config(dhcp6_get_parser(), "prefix_"), NULL));

    option_23_info_update(pool, dns);
exit:
    return;
}