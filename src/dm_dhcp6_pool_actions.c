/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include "dm_dhcp6.h"
#include "dhcp6_dm_mngr.h"
#include "dhcp6_option_info.h"

/**
 * @brief
 * Check if the given data is empty or in in a given list.
 *
 * @param object The pool object.
 * @param param NULL
 * @param reason The action identifier.
 * @param args The given string to check.
 * @param retval Variant to store the result of the action.
 * @param priv List to find the string in.
 * @return amxd_status_ok if the action is successful.
 */
amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    SAH_TRACEZ_INFO(ME, "In check_is_empty_or_in function.");

    when_str_empty_status(GET_CHAR(args, NULL), exit, status = amxd_status_ok);

    status = amxd_action_param_check_is_in(object, param, reason,
                                           args, retval, priv);
exit:
    return status;
}

/**
 * @brief
 * Check if the given data is empty or in in a given list.
 *
 * @param object The pool object.
 * @param param NULL
 * @param reason The action identifier.
 * @param args The given string to check.
 * @param retval Variant to store the result of the action.
 * @param priv List to find the string in.
 * @return amxd_status_ok if the action is successful.
 */
amxd_status_t _check_elem_empty_or_in(UNUSED amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      amxd_action_t reason,
                                      const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* list = NULL;
    amxc_var_t* refs = (amxc_var_t*) priv;
    SAH_TRACEZ_INFO(ME, "In check_elem_empty_or_in function.");

    amxc_var_new(&list);

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);
    when_true_trace(amxc_var_type_of(refs) != AMXC_VAR_ID_LIST, exit, ERROR, "Only list can be given for references");

    when_str_empty_status(GET_CHAR(args, NULL), exit, status = amxd_status_ok);
    amxc_var_convert(list, args, AMXC_VAR_ID_LIST);

    amxc_var_for_each(arg, list) {
        bool in = false;
        amxc_var_for_each(ref, refs) {
            in |= strcmp(GET_CHAR(arg, NULL), GET_CHAR(ref, NULL)) == 0;
            if(in) {
                break;
            }
        }
        when_false_trace(in, exit, ERROR, "%s not in list", GET_CHAR(arg, NULL));
    }
    status = amxd_status_ok;
exit:
    amxc_var_delete(&list);
    return status;
}

/**
 * @brief
 * Clean all the data and update th UCI file when a pool is removed.
 *
 * @param pool The pool which is going to be removed.
 * @return amxd_status_ok if the pool can be removed.
 */
static amxd_status_t remove_pool(amxd_object_t* pool) {
    amxd_status_t status = amxd_status_unknown_error;
    char* controller = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t ret;
    amxc_var_t data;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    when_null_trace(pool, exit, ERROR, "Cannot get pool data.");
    server_info_clear((server_info_t*) pool->priv);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &data, PARAMS, NULL);
    when_null_trace(params, exit, ERROR, "Unable to set parameter to data variant.");
    status = amxd_object_get_params(pool, params, amxd_dm_access_protected);
    when_failed_trace(status, exit, ERROR, "Cannot get parameter from pool object.");
    status = set_alias(params);
    when_failed_trace(status, exit, ERROR, "Unable to set alias.");
    controller = amxd_object_get_value(cstring_t, pool, "ConfigController", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
    status = amxm_execute_function(controller,
                                   MOD_DHCPV6S_UCI,
                                   "disable-pool",
                                   &data,
                                   &ret);
    when_failed_trace(status, exit, ERROR, "Unable to remove pool from controller.");
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    free(controller);
    return status;
}

/**
 * @brief
 * Remove a pool from the DHCPv6.Server object.
 *
 * @param object The DHCPv6.Server object.
 * @param param NULL
 * @param reason The action identifier.
 * @param args The data of the pool to remove.
 * @param retval Variant to store the result of the action.
 * @param priv NULL
 * @return amxd_status_ok if the pool was successfully removed.
 */
amxd_status_t _dhcp6_remove_pool(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* instance = NULL;
    const char* name = NULL;

    SAH_TRACEZ_INFO(ME, "In removing pool action handler function.");

    when_null_trace(args, exit, ERROR, "Cannot get the data of the pool to remove.");
    when_null_trace(object, exit, ERROR, "Cannot get DHCPv6.Server object.");

    name = GET_CHAR(args, "name");
    when_null_trace(name, exit, ERROR, "Cannot get object name.");

    instance = amxd_object_get_instance(object, name, 0);
    when_null_trace(instance, exit, ERROR, "Cannot get pool instance to delete.");
    status = remove_pool(instance);
    when_failed_trace(status, exit, ERROR, "Unable to remove pool configuration.");
    status = amxd_action_object_destroy(object, param, reason, args, retval, priv);
    when_failed_trace(status, exit, ERROR, "Unable to remove pool instance");
exit:
    return status;
}

amxd_status_t _dhcpv6s_pool_option_23_destroy(amxd_object_t* object,
                                              UNUSED amxd_param_t* param,
                                              amxd_action_t reason,
                                              UNUSED const amxc_var_t* const args,
                                              UNUSED amxc_var_t* const retval,
                                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    option_23_info_t* option_info = NULL;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    option_info = (option_23_info_t*) object->priv;

    when_null(option_info, exit);

    if(option_info->q_dnsserver != NULL) {
        netmodel_closeQuery(option_info->q_dnsserver);
        option_info->q_dnsserver = NULL;
    }

    option_info->obj = NULL;
    free(option_info->dynamic_addr);
    option_info->dynamic_addr = NULL;
    free(option_info->static_addr);
    option_info->static_addr = NULL;
    option_info->expected_flags.all_flags = 0;
    option_info->flags.all_flags = 0;
    free(option_info);

    status = amxd_status_ok;
exit:
    return status;
}

/**
 * @brief
 * Check if the given IANA Configuration MinAddress is lower than the MaxAddress.
 *
 * @param object The pool object.
 * @param param NULL
 * @param reason The action identifier.
 * @param args The given string to check.
 * @param retval Variant to store the result of the action.
 * @param priv List to find the string in.
 * @return amxd_status_ok if the action is successful.
 */
amxd_status_t _is_min_addr_lower_than_max(amxd_object_t* object,
                                          UNUSED amxd_param_t* param,
                                          amxd_action_t reason,
                                          UNUSED const amxc_var_t* const args,
                                          UNUSED amxc_var_t* const retval,
                                          UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    uint64_t min_ipv6_dec = 0;
    uint64_t max_ipv6_dec = 0;
    const char* min_addr = NULL;
    const char* max_addr = NULL;

    amxc_var_init(&params);

    SAH_TRACEZ_INFO(ME, "In is_min_addr_lower_then_max function");
    when_true_status(reason != action_object_validate, exit, status = amxd_status_function_not_implemented);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    min_addr = GET_CHAR(&params, "MinAddress");
    when_str_empty_status(min_addr, exit, status = amxd_status_ok);
    max_addr = GET_CHAR(&params, "MaxAddress");
    when_str_empty_status(max_addr, exit, status = amxd_status_ok);

    min_ipv6_dec = ipv6_lower_64_to_decemical(min_addr);
    max_ipv6_dec = ipv6_lower_64_to_decemical(max_addr);

    status = amxd_status_invalid_value;
    when_true_trace(min_ipv6_dec > max_ipv6_dec, exit, ERROR, "The MinAddress is greater than the MaxAddrress");

    status = amxd_status_ok;
exit:
    amxc_var_clean(&params);
    return status;
}