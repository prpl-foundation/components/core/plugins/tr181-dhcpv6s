/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "dhcp6_dm_mngr.h"
#include "dm_dhcp6.h"

/**
 * @brief
 * Set up the server information private data and configure the pool to be enable.
 * It also enable DNS and NTP server if they are available.
 *
 * @param pool The amxd pool object.
 * @return amxd_status_ok if everything is well configured
 */
amxd_status_t enable_pool(amxd_object_t* pool) {
    amxd_status_t status = amxd_status_ok;
    server_info_t* info = NULL;

    when_null_trace(pool, exit, ERROR, "Cannot get pool object.");

    if(pool->priv == NULL) {
        server_info_create(pool);
    }

    info = (server_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "Cannot get server info from pool object.");
    info->flags.bit.enable = 1;
    server_info_enable(pool);
exit:
    return status;
}

/**
 * @brief
 * Clear the server information private data and configure the pool to be disable.
 * It also disable DNS and NTP server.
 *
 * @param pool The amxd pool object.
 * @return amxd_status_ok if everything for the pool is close.
 */
amxd_status_t disable_pool(amxd_object_t* pool) {
    amxd_status_t status = amxd_status_unknown_error;
    server_info_t* info = NULL;
    when_null_trace(pool, exit, ERROR, "Cannot get pool object.");
    if(pool->priv == NULL) {
        server_info_create(pool);
    }
    info = (server_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "Cannot get server info from pool object.");
    info->flags.bit.enable = 0;
    status = sync_pool_status(pool);
    server_info_disable(pool);
    when_failed_trace(status, exit, ERROR, "Cannot sync status.");
exit:
    return status;
}

amxd_status_t dm_mngr_update_passthrough(amxd_object_t* option, const char* value) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "Updating %s.PassthroughClientValue to '%s'", amxd_object_get_path(option, AMXD_OBJECT_NAMED), value);

    amxd_trans_init(&trans);

    when_null_trace(option, exit, ERROR, "Cannot get option object.");
    when_null_trace(value, exit, ERROR, "Cannot get option value.");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, option);
    amxd_trans_set_value(cstring_t, &trans, "PassthroughClientValue", value);
    status = amxd_trans_apply(&trans, dhcp6_get_dm());
    when_failed_trace(status, exit, ERROR, "Cannot update PassthroughClientValue.");
exit:
    amxd_trans_clean(&trans);
    return status;
}

void dm_mngr_update_option_23(amxd_object_t* pool, amxd_object_t* dns) {
    option_23_info_t* option_info = NULL;
    amxd_object_t* option_23 = NULL;
    amxc_string_t addrs;
    amxc_string_t hex_value;
    amxd_trans_t trans;
    amxc_var_t addr_list;

    amxc_string_init(&addrs, 0);
    amxc_string_init(&hex_value, 0);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_var_init(&addr_list);

    when_null_trace(pool, exit, ERROR, "No pool given to update option 23");
    when_null_trace(dns, exit, ERROR, "No dns object given for option 23");

    option_info = (option_23_info_t*) dns->priv;

    when_null_trace(option_info, exit, ERROR, "No info given for option 23");

    when_false_trace(option_info->expected_flags.all_flags == option_info->flags.all_flags, exit, INFO, "Not all address obtained, DM is not updated");

    if(option_info->dynamic_addr != NULL) {
        amxc_string_appendf(&addrs, "%s", option_info->dynamic_addr);
    }
    if(option_info->static_addr != NULL) {
        amxc_string_appendf(&addrs, "%s", option_info->static_addr);
    } else {
        // remove last ','
        amxc_string_shrink(&addrs, 1);
    }

    amxd_trans_select_object(&trans, dns);
    amxd_trans_set_value(cstring_t, &trans, "DNSServers", amxc_string_get(&addrs, 0));
    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot update DNSServers with %s", amxc_string_get(&addrs, 0));

    amxd_trans_clean(&trans);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, pool);

    amxc_string_csv_to_var(&addrs, &addr_list, NULL);
    amxc_var_for_each(addr, &addr_list) {
        struct in6_addr ipv6tmp;
        inet_pton(AF_INET6, GET_CHAR(addr, NULL), &ipv6tmp);
        for(int i = 0; i < 16; i++) {
            amxc_string_appendf(&hex_value, "%02x", ipv6tmp.s6_addr[i]);
        }
    }

    option_23 = amxd_object_findf(pool, "Option.[Tag == 23].");
    if(!str_empty(amxc_string_get(&hex_value, 0))) {
        if(option_23 != NULL) {
            amxd_trans_select_object(&trans, option_23);
        } else {
            amxd_trans_select_pathf(&trans, ".Option.");
            amxd_trans_add_inst(&trans, 0, NULL);
            amxd_trans_set_value(uint16_t, &trans, "Tag", 23);
        }
        amxd_trans_set_value(cstring_t, &trans, "Value", amxc_string_get(&hex_value, 0));
        amxd_trans_set_value(bool, &trans, "Enable", true);
    } else {
        if(option_23 != NULL) {
            amxd_trans_select_pathf(&trans, ".Option.");
            amxd_trans_del_inst(&trans, amxd_object_get_index(option_23), NULL);
        } else {
            goto exit;
        }
    }
    when_failed_trace(amxd_trans_apply(&trans, dhcp6_get_dm()), exit, ERROR, "Cannot update option 23");
exit:
    amxc_string_clean(&addrs);
    amxc_string_clean(&hex_value);
    amxc_var_clean(&addr_list);
    amxd_trans_clean(&trans);
    return;
}

uint64_t ipv6_lower_64_to_decemical(const char* ipv6_str) {
    uint64_t lower_64_bits = 0;
    struct in6_addr ipv6;

    when_str_empty(ipv6_str, exit);
    when_true_trace(1 != inet_pton(AF_INET6, ipv6_str, &ipv6), exit, ERROR, "Invalid IPv6 address");

    for(int i = 8; i < 16; i++) {
        lower_64_bits = (lower_64_bits << 8) | ipv6.s6_addr[i];
    }

exit:
    return lower_64_bits;
}