/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "helper_leases.h"
#include "test_dm.h"

#include "dhcp6_dm_mngr.h"

// private helper functions


// public helper functions
void send_leases_and_sync(const char* intf_name, const char* client_duid, const char* addr, const char* pref, int delta) {
    amxc_var_t data;
    amxc_var_t* leases = NULL;
    amxc_var_t* client = NULL;
    amxc_var_t* options = NULL;
    amxc_var_t* addresses = NULL;
    amxc_var_t* address = NULL;
    amxc_var_t* prefixes = NULL;
    amxc_var_t* prefix = NULL;
    amxc_ts_t valide_time;
    amxc_ts_t prefered_time;
    int status = 1;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_ts_now(&valide_time);
    valide_time.sec = delta;
    amxc_ts_now(&prefered_time);

    leases = amxc_var_add_key(amxc_htable_t, &data, intf_name, NULL);

    client = amxc_var_add_key(amxc_htable_t, leases, client_duid, NULL);

    options = amxc_var_add_key(amxc_htable_t, client, "options", NULL);

    amxc_var_add_key(cstring_t, options, "39", "boris-ThinkPad-13-2nd-Gen.com");
    amxc_var_add_key(cstring_t, options, "1", "test-option");

    addresses = amxc_var_add_key(amxc_llist_t, client, "ipv6-addr", NULL);

    address = amxc_var_add(amxc_htable_t, addresses, NULL);

    amxc_var_add_key(cstring_t, address, "address", addr);
    amxc_var_add_key(uint32_t, address, "prefix-length", 0);
    amxc_var_add_key(amxc_ts_t, address, "valid-lifetime", &valide_time);
    amxc_var_add_key(amxc_ts_t, address, "preferred-lifetime", &prefered_time);

    prefixes = amxc_var_add_key(amxc_llist_t, client, "ipv6-prefix", NULL);

    prefix = amxc_var_add(amxc_htable_t, prefixes, NULL);

    amxc_var_add_key(cstring_t, prefix, "address", pref);
    amxc_var_add_key(uint32_t, prefix, "prefix-length", 32);
    amxc_var_add_key(amxc_ts_t, prefix, "valid-lifetime", &valide_time);
    amxc_var_add_key(amxc_ts_t, prefix, "preferred-lifetime", &valide_time);


    status = dm_mngr_set_leases(NULL,
                                &data,
                                NULL);
    assert_int_equal(status, 0);
    amxc_var_clean(&data);
    amxut_bus_handle_events();
}

void assert_leases(const char* intf_alias, int client_nb, const char* client_duid, int addr_nb, const char* addr, const char* pref, int delta) {
    char client_path[512];
    char lease_path[1024];
    char prefix_path[1024];
    char prefix[128];
    amxc_ts_t valide_time;

    amxc_ts_now(&valide_time);
    valide_time.sec = delta;

    snprintf(client_path, 1024, "DHCPv6Server.Pool.%s.Client.%d.", intf_alias, client_nb);
    snprintf(lease_path, 1024, "%sIPv6Address.%d.", client_path, addr_nb);
    snprintf(prefix_path, 1024, "%sIPv6Prefix.%d.", client_path, addr_nb);
    snprintf(prefix, 128, "%s/32", pref);

    assert_dm_object_exist(client_path);
    assert_dm_object_exist(lease_path);
    assert_dm_object_exist(prefix_path);

    assert_dm_param_equal(bool, client_path, "Active", true);
    assert_dm_param_equal(cstring_t, client_path, "DUID", client_duid);

    assert_dm_param_equal(cstring_t, lease_path, "IPAddress", addr);
    assert_dm_param_equal(cstring_t, prefix_path, "Prefix", prefix);


    assert_dm_param_equal(amxc_ts_t, lease_path, "ValidLifetime", &valide_time);
    assert_dm_param_equal(amxc_ts_t, prefix_path, "ValidLifetime", &valide_time);
}