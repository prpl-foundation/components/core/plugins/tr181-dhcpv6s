/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_dns.h"


#define COMPARE(DBG, VAL_A, VAL_B) do { \
        amxc_var_t* a = (amxc_var_t*) VAL_A; \
        amxc_var_t* b = (amxc_var_t*) VAL_B; \
        int result = -1; \
        print_message("<" DBG " from plugin>\n"); \
        amxc_var_dump(a, 1); \
        print_message("<" DBG " from unit test>\n"); \
        amxc_var_dump(b, 1); \
        if((amxc_var_compare(a, b, &result) != 0) || \
           (result != 0)) { \
            return 0; \
        } \
} while(0);

//helper

int host_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    COMPARE("dns host", value, check_value_data);
    return 1;
}

void set_expected_dns_set_arg(amxc_var_t* expected_args, const char* intf, const char* ip, bool update) {
    amxc_var_init(expected_args);
    amxc_var_set_type(expected_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, expected_args, "Enable", true);
    amxc_var_add_key(bool, expected_args, "Exclusive", true);
    amxc_var_add_key(cstring_t, expected_args, "IPAddresses", ip);
    amxc_var_add_key(cstring_t, expected_args, "Interface", intf);
    if(update) {
        amxc_var_add_key(cstring_t, expected_args, "Alias", "host-1");
    } else {
        amxc_var_add_key(cstring_t, expected_args, "Name", "boris-ThinkPad-13-2nd-Gen");
        amxc_var_add_key(cstring_t, expected_args, "Origin", "DHCPv6");
    }
    return;
}

void set_expected_dns_remove_arg(amxc_var_t* expected_args) {
    amxc_var_init(expected_args);
    amxc_var_set_type(expected_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, expected_args, "Alias", "host-1");
    return;
}

// mock function
amxd_status_t _SetHost(UNUSED amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    check_expected(args);
    amxc_var_set(cstring_t, ret, "host-1"); // return Alias
    return amxd_status_ok;
}

amxd_status_t _RemoveHost(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    check_expected(args);
    return amxd_status_ok;
}
