/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "../common/common.h"
#include "../common/test_dm.h"
#include "../common/helper_leases.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/mock_dns.h"
#include "test_methods.h"

void test_set_leases(UNUSED void** state) {
    amxc_var_t expected_args;
    amxc_var_t expected_args_2;
    amxc_var_t expected_args_3;
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    set_expected_dns_set_arg(&expected_args, "Device.Logical.Interface.2.", "2001::100", 0);
    expect_check(_SetHost, args, host_equal_check, &expected_args);
    set_expected_dns_set_arg(&expected_args_2, "Device.Logical.Interface.2.", "2001::100", 1);
    expect_check(_SetHost, args, host_equal_check, &expected_args_2);
    send_leases_and_sync("dummy-interface", "42", "2001::100", "2001::", 0);
    assert_leases("lan", 1, "42", 1, "2001::100", "2001::", 0);
    amxc_var_clean(&expected_args);
    amxc_var_clean(&expected_args_2);
    // update of the lease
    set_expected_dns_set_arg(&expected_args, "Device.Logical.Interface.2.", "2001::50", 1);
    expect_check(_SetHost, args, host_equal_check, &expected_args);
    send_leases_and_sync("dummy-interface", "42", "2001::50", "2002::", 0);
    assert_leases("lan", 1, "42", 2, "2001::50", "2002::", 0);
    amxc_var_clean(&expected_args);
    send_leases_and_sync("dummy-interface", "42", "2001::50", "2002::", 100);
    assert_leases("lan", 1, "42", 2, "2001::50", "2002::", 100);

    set_expected_dns_remove_arg(&expected_args);
    expect_check(_RemoveHost, args, host_equal_check, &expected_args);
    set_expected_dns_set_arg(&expected_args_2, "Device.Logical.Interface.2.", "2001::110", 0);
    expect_check(_SetHost, args, host_equal_check, &expected_args_2);
    set_expected_dns_set_arg(&expected_args_3, "Device.Logical.Interface.2.", "2001::110", 1);
    expect_check(_SetHost, args, host_equal_check, &expected_args_3);
    send_leases_and_sync("dummy-interface", "45", "2001::110", "2001::", 0);
    assert_leases("lan", 2, "45", 1, "2001::110", "2001::", 0);
    amxc_var_clean(&expected_args);
    amxc_var_clean(&expected_args_2);
    amxc_var_clean(&expected_args_3);
}

void test_disable_pool(UNUSED void** state) {
    amxc_var_t expected_args;
    amxc_var_t expected_args_2;

    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    set_expected_dns_set_arg(&expected_args, "Device.Logical.Interface.2.", "2001::100", 0);
    expect_check(_SetHost, args, host_equal_check, &expected_args);
    set_expected_dns_set_arg(&expected_args_2, "Device.Logical.Interface.2.", "2001::100", 1);
    expect_check(_SetHost, args, host_equal_check, &expected_args_2);
    send_leases_and_sync("dummy-interface", "42", "2001::100", "2001::", 0);
    assert_leases("lan", 1, "42", 1, "2001::100", "2001::", 0);
    amxc_var_clean(&expected_args);
    amxc_var_clean(&expected_args_2);

    set_expected_dns_remove_arg(&expected_args);
    expect_amxm_execute_function_module_nb("disable-pool", 1);
    expect_check(_RemoveHost, args, host_equal_check, &expected_args);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "Enable", false);
    assert_dm_null_object("DHCPv6Server.Pool.lan.Client.1.");
    amxc_var_clean(&expected_args);
}