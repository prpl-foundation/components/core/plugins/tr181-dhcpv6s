/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "../common/common.h"
#include "../common/test_dm.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "test_pool_events.h"

void test_pool_enable(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("disable-pool", 1);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "Enable", false);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Disabled");

    // each call back send an event to the module and enabling send an event
    expect_amxm_execute_function_module_nb("disable-pool", 1);
    expect_amxm_execute_function_module_nb("enable-pool", 2);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "IANAEnable", false);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "IANAEnable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    // IAPDEnable is false by default on lan pool
    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "IAPDEnable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(bool, "DHCPv6Server.Pool.lan.", "IAPDEnable", false);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");
}

void test_pool_interface(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("disable-pool", 2);
    assert_dm_set_param_and_sync(csv_string_t, "DHCPv6Server.Pool.lan.", "Interface", "test");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Error_Misconfigured");

    // one more disable as interface for the shutdown of the backend
    expect_amxm_execute_function_module_nb("disable-pool", 2);
    expect_amxm_execute_function_module_nb("enable-pool", 2);
    assert_dm_set_param_and_sync(csv_string_t, "DHCPv6Server.Pool.lan.", "Interface", "Device.Logical.Interface.3.");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");
}

void test_pool_options(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_add_pool_option_and_sync("DHCPv6Server.Pool.lan.Option.", 1, "test-value", "");
    assert_dm_param_equal(uint32_t, "DHCPv6Server.Pool.lan.Option.cpe-Option-2.", "Tag", 1);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.cpe-Option-2.", "Value", "test-value");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(cstring_t, "DHCPv6Server.Pool.lan.Option.cpe-Option-2.", "Value", "test-value-2");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.cpe-Option-2.", "Value", "test-value-2");
}

void test_pool_helper_option_23(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(cstring_t, "DHCPv6Server.Pool.lan.DNS.", "DNSMode", "LLA,GUA");

    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.DNS.", "DNSServers", "fe80::01,2102::65");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 23].", "Value", "fe80000000000000000000000000000121020000000000000000000000000065");

    assert_dm_set_param_and_sync(cstring_t, "DHCPv6Server.Pool.lan.DNS.", "DNSMode", "");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.DNS.", "DNSServers", "fe80::01,2102::65");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 23].", "Value", "fe80000000000000000000000000000121020000000000000000000000000065");
}

void test_pool_options_passthrough(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "Enable", true);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.", "Status", "Enabled");

    expect_amxm_execute_function_module_nb("enable-pool", 2);
    assert_dm_add_pool_option_and_sync("DHCPv6Server.Pool.lan.Option.", 18, "test-value", "DHCPv6Client.Client.lan.");
    assert_dm_param_equal(uint32_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "Tag", 18);
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "Value", "test-value");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "test-value-1");

    // Update ReceivedOption value from DHCPv6Client
    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(cstring_t, "DHCPv6Client.Client.lan.ReceivedOption.cpe-ReceivedOption-1.", "Value", "test-value-2");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "test-value-2");

    // Delete ReceivedOption from DHCPv6Client
    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_remove_instance_and_sync("DHCPv6Client.Client.lan.ReceivedOption.", "cpe-ReceivedOption-1");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "");

    // Restore ReceivedOption from DHCPv6Client but with different tag (nothing should happen)
    assert_dm_add_received_option_and_sync("DHCPv6Client.Client.lan.ReceivedOption.", "cpe-ReceivedOption-2", 15, "test-value-3");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "");

    // Delete it again and restore it with correct tag
    assert_dm_remove_instance_and_sync("DHCPv6Client.Client.lan.ReceivedOption.", "cpe-ReceivedOption-2");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "");
    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_add_received_option_and_sync("DHCPv6Client.Client.lan.ReceivedOption.", "cpe-ReceivedOption-3", 18, "test-value-4");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "test-value-4");

    // Set PassthroughClientValue to not yet existing DHCPv6Client
    expect_amxm_execute_function_module_nb("enable-pool", 2);
    assert_dm_set_param_and_sync(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClient", "DHCPv6Client.Client.3.");
    assert_dm_param_equal(cstring_t, "DHCPv6Server.Pool.lan.Option.[Tag == 18].", "PassthroughClientValue", "");
}

void test_pool_iana_config(UNUSED void** state) {
    assert_dm_param_equal(bool, "DHCPv6Server.Pool.lan.", "IANAEnable", true);
    assert_dm_param_equal(int32_t, "DHCPv6Server.Pool.lan.IANAConfig.", "LeaseTime", 3600);

    expect_amxm_execute_function_module_nb("enable-pool", 1);
    assert_dm_set_param_and_sync(int32_t, "DHCPv6Server.Pool.lan.IANAConfig.", "LeaseTime", 1000);

    assert_dm_param_equal(int32_t, "DHCPv6Server.Pool.lan.IANAConfig.", "LeaseTime", 1000);
}