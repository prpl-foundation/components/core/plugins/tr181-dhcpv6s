# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.20.10 - 2024-09-30(18:21:30 +0000)

### Other

- - [tr181-dhcpv6server] DSCP value in dhcpv6 packet

## Release v0.20.9 - 2024-09-25(09:43:33 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds
- [dhcpv6 server] Option 17 vendor specific option should be supported

## Release v0.20.9 - 2024-09-10(07:21:59 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v0.20.8 - 2024-07-08(06:56:34 +0000)

## Release v0.20.7 - 2024-06-17(10:07:43 +0000)

### Other

- - amx plugin should not run as root user

## Release v0.20.6 - 2024-05-30(13:00:30 +0000)

### Other

- [DHCPv6] IP Start, End range and lease Time TR181 Data models are not available: part 2, functional part

## Release v0.20.5 - 2024-05-30(11:41:22 +0000)

### Other

- create configuration based on the number of bridges

## Release v0.20.4 - 2024-04-10(10:09:16 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.20.3 - 2024-03-29(14:59:25 +0000)

### Fixes

- [tr181-pcm] Saved and defaults odl should not be included in the backup files

## Release v0.20.2 - 2024-03-18(14:41:23 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.20.1 - 2024-02-23(11:05:10 +0000)

### Fixes

- Data model parameter needed for IPv6 Lifetime Parameter

## Release v0.20.0 - 2024-02-05(16:22:06 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.19.0 - 2024-02-02(13:06:23 +0000)

### New

- CLONE - [IPv6][Server]DHCPv6 Option 23 announced on LAN is only LLA iso GUA+LLA

## Release v0.18.0 - 2024-01-17(09:33:08 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.17.0 - 2023-12-05(12:20:01 +0000)

### New

- [DHCPv6] IP Start, End range and least Time TR181 Data models are not available: part 1, provide DataModel

## Release v0.16.1 - 2023-10-13(14:12:44 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.16.0 - 2023-09-29(11:36:25 +0000)

### New

- [amxrt][no-root-user][capability drop] tr181-dhcpv6s plugin must be adapted to run as non-root and lmited capabilities

## Release v0.15.2 - 2023-09-09(06:39:49 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v0.15.1 - 2023-09-07(10:17:22 +0000)

### Fixes

- [dhcvpv6-manager] memleak on 0.7.0

## Release v0.15.0 - 2023-07-17(12:26:15 +0000)

### New

- [tr181-logical][LAN mib] The Lan mib must expose the domainSearchList parameter

## Release v0.14.0 - 2023-06-15(08:36:38 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v0.13.0 - 2023-06-01(10:19:20 +0000)

### New

- add ACLs permissions for cwmp user

## Release v0.12.2 - 2023-05-23(15:20:41 +0000)

## Release v0.12.1 - 2023-05-16(08:04:43 +0000)

### Fixes

- [tr181-dhcpv6server] By default Clients cannot get an address from DHCPv6Server

## Release v0.12.0 - 2023-05-12(10:26:26 +0000)

### New

- [tr181-dhcpv6server] wrong behavior when disabling both IA_PD and IA_NA

## Release v0.11.1 - 2023-04-20(13:12:39 +0000)

### Fixes

- service protocol should be integers

## Release v0.11.0 - 2023-03-13(15:56:19 +0000)

### Fixes

- [DHCPv6] Split client and server plugin

## Release v0.10.2 - 2023-03-09(09:19:23 +0000)

### Other

- tr181-components: missing explicit dependency on rpcd service providing ubus uci backend
- [Config] enable configurable coredump generation

## Release v0.10.1 - 2023-01-31(13:54:30 +0000)

### Other

- [CI] Update dependencies

## Release v0.10.0 - 2023-01-31(11:49:05 +0000)

### New

- update DNS data model with hosts from DHCP server pool

## Release v0.9.18 - 2023-01-31(08:21:10 +0000)

### Fixes

- No GUA Address received after IA_NA request is sent

## Release v0.9.17 - 2023-01-09(09:59:14 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.9.16 - 2022-12-28(09:16:17 +0000)

### Fixes

- [dhcpv6-manager] send event when status is updated

## Release v0.9.15 - 2022-12-22(16:38:55 +0000)

### Fixes

- [dhcpv6s-manager] enable guest pool

## Release v0.9.14 - 2022-12-09(09:20:21 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.9.13 - 2022-11-25(14:28:56 +0000)

### Fixes

- [DHCPv6 server] Only check Enable for the open and close of the firewall

## Release v0.9.12 - 2022-11-15(18:19:39 +0000)

### Fixes

- [DHCPv6 server] change is up query with an lla check

## Release v0.9.11 - 2022-10-24(11:47:34 +0000)

### Fixes

- [TR181-DHCPv6] memory leak detected

## Release v0.9.10 - 2022-09-13(06:46:10 +0000)

### Changes

- tests hint regression with dhcpv6 client and/or server data model not present/responding

## Release v0.9.9 - 2022-09-08(08:37:52 +0000)

### Changes

- tests hint regression with dhcpv6 client and/or server data model not present/responding

## Release v0.9.8 - 2022-09-08(06:44:05 +0000)

### Fixes

- Switch clients handling from "delete all - add all" to add, update and remove

## Release v0.9.7 - 2022-09-05(09:06:26 +0000)

### Fixes

- Unable to add firewall rule returned -1

## Release v0.9.6 - 2022-07-29(07:38:05 +0000)

### Fixes

- dhcpv servers fails to create cpe-dhcpvXs-* firewall services on firstboot

## Release v0.9.5 - 2022-07-28(13:50:43 +0000)

### Fixes

- [tr181-dhcpv6s] Rework odhcpd_poll_leases

## Release v0.9.4 - 2022-07-28(12:07:45 +0000)

### Fixes

- DHCP v4/v6 managers started alongside clients

## Release v0.9.3 - 2022-07-07(06:58:06 +0000)

### Fixes

- LAN Client does not get IPv4 address on first boot

## Release v0.9.2 - 2022-06-27(11:11:31 +0000)

### Fixes

- [dhcpv6s-manager] Firewall not creating rules for dhcpv6 server

## Release v0.9.1 - 2022-06-23(11:25:29 +0000)

### Fixes

- [tr181-dhcpv4client] fials to load datamodel at startup

## Release v0.9.0 - 2022-06-17(09:20:46 +0000)

### New

- [prpl][amx][pcm][dhcpv6server] Definition of Upgrade Persistent DHCPv6 Server Configuration

## Release v0.8.2 - 2022-05-30(07:41:56 +0000)

### Other

- [DHCPv6-manager] DNS servers should be forwarded to client. [add]

## Release v0.8.1 - 2022-05-16(06:11:18 +0000)

### Other

- [tr181-dhcpv6s] IPv6Prefix listening [add]

## Release v0.8.0 - 2022-05-04(15:12:37 +0000)

### New

- [tr181-dhcpv6s] Interface status listening

## Release v0.7.1 - 2022-03-24(11:08:13 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.7.0 - 2022-03-03(12:46:11 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v0.6.12 - 2022-02-25(10:57:23 +0000)

### Other

- Enable core dumps by default

## Release v0.6.11 - 2021-11-29(16:25:41 +0000)

### Fixes

- variant cleanup

## Release v0.6.10 - 2021-11-24(09:32:37 +0000)

### Changes

- enable dhcpv6 guest pool by default

## Release v0.6.9 - 2021-11-18(14:39:13 +0000)

### Fixes

- fix pool update action

## Release v0.6.8 - 2021-11-09(08:27:32 +0000)

### Fixes

- extend fix to all parameters

## Release v0.6.7 - 2021-11-08(12:51:15 +0000)

### Fixes

- fix writting interface alias to UCI

## Release v0.6.6 - 2021-11-08(09:35:25 +0000)

### Changes

- set default interface for guest pool

## Release v0.6.5 - 2021-11-04(08:21:10 +0000)

### Fixes

- fix writing pool to UCI

## Release v0.6.4 - 2021-10-25(07:27:12 +0000)

### Changes

- enable logging

## Release v0.6.3 - 2021-10-23(07:06:44 +0000)

### Fixes

- [TR181-DHCPv6 Server] use common datamodel with DHCPv6 client

## Release v0.6.2 - 2021-10-22(08:14:48 +0000)

### Fixes

- firewall: open port when being configured from uci

## Release v0.6.1 - 2021-10-21(11:37:35 +0000)

### Fixes

- set Client and subobjects as read-only

## Release v0.6.0 - 2021-10-21(10:59:34 +0000)

### New

- use 'valid' value from lease

## Release v0.5.1 - 2021-10-08(08:02:16 +0000)

### Changes

- accept multiple lease ips

## Release v0.5.0 - 2021-10-05(06:50:43 +0000)

### New

- integrate with firewall

## Release v0.4.0 - 2021-09-23(14:51:58 +0000)

### New

- odhcpd module (IPv6 leases)

## Release v0.3.0 - 2021-09-09(10:17:44 +0000)

### New

- Initial separation of concerns

## Release v0.2.0 - 2021-08-23(14:31:25 +0000)

### New

- odl: add defaults directory

## Release v0.1.0 - 2021-08-13(10:21:05 +0000)

### New

- Initial import

