/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_DHCPV6S_NETLINK_H__)
#define __MOD_DHCPV6S_NETLINK_H__

#include "netmodel/client.h"
#include <amxp/amxp_signal.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct info_flags {
    uint8_t enable : 1;
    uint8_t intf_exist : 1;
    uint8_t have_prefix : 1;
    uint8_t serv_ok : 1;
    uint8_t firewall_up : 1;
    uint8_t rsvd : 3;

};

typedef struct _server_info {
    netmodel_query_t* q_prefix;
    netmodel_query_t* q_name;
    amxd_object_t* obj;
    char* intf_path;
    char* intf_name;
    char* fw_alias;
    const char* pool_name;
    union {
        uint8_t all_flags;
        struct info_flags bit;
    } flags;
    amxc_var_t* last_client_htable;
} server_info_t;

struct option_23_flags {
    uint8_t static_ok : 1;
    uint8_t lla_ok : 1;
    uint8_t gua_ok : 1;
    uint8_t rsvd : 5;

};

typedef struct _option_23_info {
    netmodel_query_t* q_dnsserver;
    amxd_object_t* obj;
    char* dynamic_addr;
    char* static_addr;
    union {
        uint8_t all_flags;
        struct option_23_flags bit;
    } expected_flags;
    union {
        uint8_t all_flags;
        struct option_23_flags bit;
    } flags;
} option_23_info_t;

void server_info_clear(server_info_t* info);
void server_info_enable(amxd_object_t* obj);
void server_info_disable(amxd_object_t* obj);
void server_info_create(amxd_object_t* obj);

void option_23_info_update(amxd_object_t* pool, amxd_object_t* option_23);

#ifdef __cplusplus
}
#endif

#endif // __MOD_DHCPV6S_NETLINK_H__