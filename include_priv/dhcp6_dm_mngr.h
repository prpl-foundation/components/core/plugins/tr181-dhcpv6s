/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DHCP6_DM_MNGR_H__)
#define __DHCP6_DM_MNGR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "dhcp6_priv.h"
#include "dm_dhcp6.h"
#include "dhcp6_server_info.h"
#include "dhcp6_option_info.h"

int PRIVATE dhcp6s_dm_mngr_init(void);
int PRIVATE dhcp6s_dm_mngr_clean(void);

// manager related function
int PRIVATE dm_mngr_create(amxd_object_t* object,
                           const char* alias,
                           amxc_var_t* parameters,
                           amxc_var_t* ret);

// utilities function
const char* PRIVATE dm_mngr_get_interface_alias(const char* interface);
uint32_t PRIVATE dm_mngr_pool_interface_matches(const char* interface);
amxd_status_t set_alias(amxc_var_t* params);
uint64_t ipv6_lower_64_to_decemical(const char* ipv6_str);


// pool related
amxd_status_t PRIVATE disable_pool(amxd_object_t* pool);
amxd_status_t PRIVATE enable_pool(amxd_object_t* pool);
amxd_status_t PRIVATE sync_pool_status(amxd_object_t* pool);
void dm_mngr_update_option_23(amxd_object_t* pool, amxd_object_t* dns);
amxd_status_t dm_mngr_update_passthrough(amxd_object_t* option, const char* value);

// leases related
int dm_mngr_set_leases(const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret);

int dm_remove_client(amxd_object_t* templ,
                     amxd_object_t* client,
                     void* priv);

#ifdef __cplusplus
}
#endif

#endif // __DHCP6_DM_MNGR_H__
