/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_module.h"
#include "mod_dhcpv6_odhcpd.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxb_get(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    int32_t depth,
                    amxc_var_t* ret,
                    int timeout);

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     UNUSED amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    function_called();
    assert_string_equal(object, "dhcp.");
    assert_string_equal(method, "ipv6leases");

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    amxc_var_t* first = amxc_var_add(amxc_htable_t, ret, NULL);
    amxc_var_t* device = amxc_var_add_key(amxc_htable_t, first, "device", NULL);
    amxc_var_t* interface = amxc_var_add_key(amxc_htable_t, device, "br-lan", NULL);
    amxc_var_t* leases = amxc_var_add_key(amxc_llist_t, interface, "leases", NULL);

    amxc_var_t* lease1 = amxc_var_add(amxc_htable_t, leases, NULL);
    amxc_var_add_key(cstring_t, lease1, "duid", "0abcdef123123123123123123123");
    amxc_var_t* ipv6addr1 = amxc_var_add_key(amxc_llist_t, lease1, "ipv6-addr", NULL);
    amxc_var_t* ipv6addrc1 = amxc_var_add(amxc_htable_t, ipv6addr1, NULL);
    amxc_var_add_key(cstring_t, ipv6addrc1, "address", "fd16:b52a:a535::99a");

    amxc_var_t* lease2 = amxc_var_add(amxc_htable_t, leases, NULL);
    amxc_var_add_key(cstring_t, lease2, "duid", "0abcdef123123123123123123124");
    amxc_var_t* ipv6addr2 = amxc_var_add_key(amxc_llist_t, lease2, "ipv6-prefix", NULL);
    amxc_var_t* ipv6addrc2 = amxc_var_add(amxc_htable_t, ipv6addr2, NULL);
    amxc_var_add_key(cstring_t, ipv6addrc2, "address", "fd16:b52a:a535:4::");
    amxc_var_add_key(cstring_t, ipv6addrc2, "prefix-length", "62");

    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    function_called();
    return 0;
}

int __wrap_amxb_get(UNUSED amxb_bus_ctx_t* const bus_ctx,
                    UNUSED const char* object,
                    UNUSED int32_t depth,
                    amxc_var_t* ret,
                    UNUSED int timeout) {
    amxc_var_t obj_ht;
    amxc_var_t values;
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_init(&values);
    amxc_var_init(&obj_ht);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&obj_ht, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Alias", "lan");
    amxc_var_add_key(amxc_htable_t, &obj_ht, "IP.Interface.1", amxc_var_constcast(amxc_htable_t, &values));
    amxc_var_add(amxc_htable_t, ret, amxc_var_constcast(amxc_htable_t, &obj_ht));
    amxc_var_clean(&values);
    amxc_var_clean(&obj_ht);
    return AMXB_STATUS_OK;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
    handle_events();
}

void test_call_poll(UNUSED void** state) {
    expect_function_call(__wrap_amxb_call);
    expect_function_call(__wrap_amxm_execute_function);
    odhcpd_get_leases(NULL, NULL);

}
