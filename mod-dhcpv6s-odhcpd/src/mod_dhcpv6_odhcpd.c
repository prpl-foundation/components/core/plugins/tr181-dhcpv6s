/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_dhcpv6_odhcpd.h"

static amxp_timer_t* timer;

#define DEFAULT_TIMER 10000

/**
 * @brief
 * Calculate the end of the lease time.
 *
 * @param time The end time of the lease.
 * @param seconds The lease duration in seconds.
 */
static void dm_calculate_time(amxc_ts_t* time, int32_t seconds) {
    if(seconds > 0) {
        amxc_ts_now(time);
        time->sec += seconds;
    } else {
        const char* str_exp = "9999-12-31T23:59:59Z";
        amxc_ts_parse(time, str_exp, strlen(str_exp));
    }
}

/**
 * @brief
 * Return the local leases for a selected interface.
 *
 * @param timer The timer that call back this function.
 * @param priv private information pass through calls.
 */
void odhcpd_get_leases(UNUSED amxp_timer_t* local_timer,
                       UNUSED void* priv) {
    amxc_var_t* first_table;
    amxc_var_t* device;
    amxc_var_t ret_call;
    amxb_bus_ctx_t* ctx = NULL;

    amxc_var_init(&ret_call);

    ctx = amxb_be_who_has("dhcp.");
    when_failed_trace(amxb_call(ctx, "dhcp.", "ipv6leases", NULL, &ret_call, 3), exit, ERROR, "Unable to get the leases from odhcpd.");

    first_table = amxc_var_get_first(&ret_call);
    when_null_trace(first_table, exit, ERROR, "No table in dhcp.ipv6leases().");
    device = GET_ARG(first_table, "device");
    when_null_trace(device, exit, ERROR, "No devices entry in the first table of dhcp.ipv6leases().");

    amxc_var_for_each(intf, device) {
        int status = 1;
        amxc_var_t data;
        amxc_var_t ret;
        amxc_var_t* params;

        amxc_var_init(&data);
        amxc_var_init(&ret);

        amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
        params = amxc_var_add_key(amxc_htable_t, &data, amxc_var_key(intf), NULL);

        amxc_var_for_each(lease, GET_ARG(intf, "leases")) {
            bool prefix = GET_ARG(lease, "ipv6-addr") == NULL;
            amxc_var_t* client = NULL;
            amxc_var_t* ip_addr_list = NULL;
            amxc_var_t* options = NULL;

            client = amxc_var_add_key(amxc_htable_t, params, GET_CHAR(lease, "duid"), NULL);
            when_null_trace(client, exit_lease_build, ERROR, "Unable to add %s key to return data.", GET_CHAR(lease, "duid"));
            options = amxc_var_add_key(amxc_htable_t, client, "options", NULL);
            amxc_var_add_key(cstring_t, options, "39", GET_CHAR(lease, "hostname"));
            ip_addr_list = amxc_var_add_key(amxc_llist_t, client, prefix ? "ipv6-prefix" : "ipv6-addr", NULL);
            when_null_trace(ip_addr_list, exit_lease_build, ERROR, "Unable to add ipv6-addr key to return data.");

            amxc_var_for_each(ip_addr, GET_ARG(lease, prefix ? "ipv6-prefix" : "ipv6-addr")) {
                amxc_var_t* ip_addr_cpy = amxc_var_add(amxc_htable_t, ip_addr_list, NULL);
                amxc_ts_t validt;
                amxc_ts_t preferredt;
                dm_calculate_time(&validt, GET_INT32(ip_addr, "valid-lifetime"));
                dm_calculate_time(&preferredt, GET_INT32(ip_addr, "preferred-lifetime"));
                amxc_var_add_key(cstring_t, ip_addr_cpy, "address", GETP_CHAR(ip_addr, "address"));
                amxc_var_add_key(uint32_t, ip_addr_cpy, "prefix-length", GET_UINT32(lease, "prefix-length"));
                amxc_var_add_key(amxc_ts_t, ip_addr_cpy, "valid-lifetime", &validt);
                amxc_var_add_key(amxc_ts_t, ip_addr_cpy, "preferred-lifetime", &preferredt);
            }
        }

        status = amxm_execute_function("self",
                                       MOD_DM_MNGR,
                                       "set-leases",
                                       &data,
                                       &ret);
exit_lease_build:
        if(status != 0) {
            SAH_TRACEZ_WARNING(ME, "Cannot update lease for %s.", amxc_var_key(intf));
        }
        amxc_var_clean(&data);
        amxc_var_clean(&ret);
    }
exit:
    amxp_timer_start(timer, DEFAULT_TIMER);
    amxc_var_clean(&ret_call);
    return;
}

/**
 * @brief
 * Start and register functions for the odhcpd module.
 * It also start the polling of the odhcpd server for leases.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dhcpv6_odhcpd_start(void) {
    amxp_timer_new(&timer, odhcpd_get_leases, NULL);
    amxp_timer_start(timer, 3000);
    return 0;
}

/**
 * @brief
 * Stop and clean the odhcpd module.
 * It also stop the polling of the odhcpd server.
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR dhcpv6_odhcpd_stop(void) {
    amxp_timer_delete(&timer);
    return 0;
}
