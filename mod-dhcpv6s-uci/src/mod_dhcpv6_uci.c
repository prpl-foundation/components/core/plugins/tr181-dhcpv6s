/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <v4v6option.h>

#include "mod_dhcpv6_uci.h"
#include "ubus_uci.h"

#define ME "dhcpv6s_uci"
#define STR_EMPTY(_str) ((_str == NULL) || (_str[0] == '\0'))

/**
 * @brief
 * Parse the DHCPv6 option (Option 17) by creating option-code and option-value tuples.
 * These tuples are stored in the option_parsed variant. Add the Enterprise value to the list of options.
 *
 * @param option_list Pointer to the destination configuration variant (`amxc_var_t*`) where parsed data will be stored.
 * @param option_parsed Pointer to the parsed DHCPv6 Option 17 data (`amxc_var_t*`) containing the vendor-specific information.
 */
static void parse_option_17(amxc_var_t* option_list, amxc_var_t* option_parsed) {
    amxc_var_t* data = GET_ARG(option_parsed, "Data");
    char* enterprise = amxc_var_dyncast(cstring_t, GET_ARG(option_parsed, "Enterprise"));
    amxc_var_t vendor_option;
    amxc_var_init(&vendor_option);
    amxc_var_set_type(&vendor_option, AMXC_VAR_ID_LIST);

    when_null_trace(option_list, exit, ERROR, "The uci configuration variant list is NULL");

    amxc_var_for_each(opt, data) {
        amxc_string_t option;
        amxc_string_init(&option, 0);
        char* opt_code = amxc_var_dyncast(cstring_t, GET_ARG(opt, "Code"));
        if(STR_EMPTY(opt_code)) {
            continue;
        }
        const char* opt_data = GET_CHAR(opt, "Data");

        amxc_string_setf(&option, "%s,%s", opt_code, opt_data);
        amxc_var_add(cstring_t, &vendor_option, amxc_string_get(&option, 0));

        amxc_string_clean(&option);
        free(opt_code);
    }
    amxc_var_add_key(cstring_t, option_list, "enterprise", enterprise);
    amxc_var_move(option_parsed, &vendor_option);

exit:
    free(enterprise);
    amxc_var_clean(&vendor_option);
    return;
}

/**
 * @brief
 * Parse dhcpv6 option using libdhcpoptions
 *
 * @param option_parsed return variant that contains the parsed option
 * @param value the hex string value to parse by libdhcpoptions
 * @param tag the option tag
 */
static int parse_dhcp_option(amxc_var_t* option_parsed, const char* value, uint32_t tag) {
    unsigned char* option = NULL;
    uint32_t length = 0;
    int rv = -1;

    when_null_trace(option_parsed, exit, ERROR, "option_parsed variant is NULL");
    when_str_empty_trace(value, exit, ERROR, "Value is empty, can't parse option %u", tag);

    option = dhcpoption_option_convert2bin(value, &length);
    when_null_trace(option, exit, ERROR, "Unable to create the binary of the option");

    dhcpoption_v6parse(option_parsed, tag, length, option);
    rv = 0;
exit:
    free(option);
    return rv;
}

/**
 * @brief
 * Parse option
 *
 * @param option_list the list of dhcp options that will be written to uci
 * @param uci_conf_name Name of the uci configuration
 * @param value the hex string value to parse by libdhcpoptions
 * @param option_tag the dhcpv6 option number
 * @param is_llist the option parsed need to be convert to an uci list
 */
static int parse_option(amxc_var_t* option_list, const char* uci_conf_name, const char* value, uint8_t option_tag, bool is_llist) {
    amxc_var_t option_parsed;
    int ret = 1;

    amxc_var_init(&option_parsed);

    when_null_trace(option_list, exit, ERROR, "option_list variant is NULL");
    when_str_empty_trace(uci_conf_name, exit, ERROR, "No uci config name given");
    when_str_empty_trace(value, exit, ERROR, "Value is empty, can't parse option %d", option_tag);

    when_failed_trace(parse_dhcp_option(&option_parsed, value, option_tag), exit, ERROR, "Failed to parse option %d", option_tag);

    if(option_tag == 17) {
        parse_option_17(option_list, &option_parsed);
    }

    if(is_llist) {
        amxc_var_add_key(amxc_llist_t, option_list, uci_conf_name, amxc_var_constcast(amxc_llist_t, &option_parsed));
    } else {
        amxc_var_add_key(cstring_t, option_list, uci_conf_name, amxc_var_constcast(cstring_t, &option_parsed));
    }

    ret = 0;
exit:
    amxc_var_clean(&option_parsed);
    return ret;
}

/**
 * @brief
 * set the list of dhcpv4 option to be uci compatible.
 *
 * @param params The params from the dhcpv4 server plugin.
 * @param options_list the list of option to be configured in uci.
 * @return 0 if success
 */
static int uci_pool_set_config_dhcp_options(amxc_var_t* params, amxc_var_t* options_list) {
    int ret = 1;
    amxc_var_t* options = NULL;
    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(options_list, exit, ERROR, "List of options not initialised.");

    options = GET_ARG(params, "Option");
    amxc_var_for_each(option, options) {
        const char* value = GET_CHAR(option, "Value");
        bool enable = GET_BOOL(option, "Enable");
        uint8_t tag = GET_UINT32(option, "Tag");

        if(enable && (value != NULL)) {
            switch(tag) {
            case 23:
                ret = parse_option(options_list, "dns", value, tag, true);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option 23");
                }
                break;
            case 24:
                ret = parse_option(options_list, "domain", value, tag, true);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option 24");
                }
                break;

            case 17:
                ret = parse_option(options_list, "vendor_info", value, tag, true);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option 17");
                }
                break;
            default:
                ret = parse_option(options_list, "option", value, tag, false);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option %d", tag);
                }
            }
        }
    }

    ret = 0;
exit:
    return ret;
}

/**
 * @brief
 * Translate the DHCPv6 pool parameters to uci configuration.
 *
 * @param orig DHCPv6 pool parameters
 * @param dest uci configuration to fill .
 */
static void translate_tr181_to_uci(amxc_var_t* orig, amxc_var_t* dest) {
    amxc_var_t* var = NULL;
    char val[11] = {0};

    when_null_trace(orig, exit, ERROR, "Unable to get DHCPv6 pool parameters.");
    when_null_trace(dest, exit, ERROR, "Return variant not initialised.");

    amxc_var_add_key(cstring_t, dest, "dhcpv6", "server");
    amxc_var_add_key(bool, dest, "dns_service", 1);
    snprintf(val, sizeof(val) - 1, "%u", GET_UINT32(orig, "DSCP"));
    amxc_var_add_key(cstring_t, dest, "dhcpv6_dscp", val);

    amxc_var_add_key(bool, dest, "dhcpv6_na", GET_BOOL(orig, "IANAEnable"));
    amxc_var_add_key(bool, dest, "dhcpv6_pd", GET_BOOL(orig, "IAPDEnable"));
    if(GET_BOOL(orig, "IANAEnable")) {
        snprintf(val, sizeof(val) - 1, "%d", GET_UINT32(orig, "LeaseTime"));
        amxc_var_add_key(cstring_t, dest, "leasetime", val);
        amxc_var_add_key(cstring_t, dest, "start_ipv6", GET_CHAR(orig, "MinAddress"));
        amxc_var_add_key(cstring_t, dest, "limit_ipv6", GET_CHAR(orig, "MaxAddress"));
    }

    var = GET_ARG(orig, "dhcpv6s181alias");
    if(var != NULL) {
        const char* tr181_alias = GET_CHAR(var, NULL);
        amxc_var_add_key(cstring_t, dest, "dhcpv6s181alias", tr181_alias);
        amxc_var_add_key(cstring_t, dest, "interface", tr181_alias); // uci can't handle tr181 interface paths
    }

    var = GET_ARG(orig, "InterfaceName");
    if(var != NULL) {
        amxc_var_add_key(cstring_t, dest, "ifname", GET_CHAR(var, NULL));
    }

    when_failed_trace(uci_pool_set_config_dhcp_options(orig, dest),
                      exit, ERROR, "Failed to write DHCPv6 options to uci config");

exit:
    return;
}

/**
 * @brief
 * Delete list parameter options from the uci config.
 *
 * @param alias name of the uci section
 */
static int uci_config_delete_lists(const char* alias) {
    int retval = -1;
    amxc_var_t uci_args;
    amxc_var_t ret;

    amxc_var_init(&uci_args);
    amxc_var_init(&ret);

    when_str_empty_trace(alias, exit, ERROR, "UCI section name is empty, can't delete list options");

    amxc_var_add(cstring_t, &uci_args, "ntp");
    amxc_var_add(cstring_t, &uci_args, "dns");
    amxc_var_add(cstring_t, &uci_args, "domain");

    retval = uci_call("delete", "dhcp", alias, "dhcp", &uci_args, &ret);
    when_failed_trace(retval, exit, ERROR, "Unable to delete dhcpv6 list parameters.");

    retval = 0;
exit:
    amxc_var_clean(&uci_args);
    amxc_var_clean(&ret);
    return retval;
}

/**
 * @brief
 * Enable DHCPv6 pool in the uci config.
 * It also enable DNS and NTP servers configuration as no DHCP option can be passed.
 *
 * @param function_name Name of the function called.
 * @param args Parameters from the amxd pool object.
 * @param ret Return variant.
 * @return int 0 on success.
 */
static int uci_enable_pool(UNUSED const char* function_name,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t* params = NULL;
    const char* alias = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    params = GET_ARG(args, PARAMS);
    when_null_trace(params, exit, ERROR, "Unable to get pool parameters.");
    alias = GET_CHAR(params, "Alias");
    when_null_trace(alias, exit, ERROR, "Unable to get Alias.");

    translate_tr181_to_uci(params, &uci_args);

    retval = uci_config_delete_lists(alias);
    when_failed_trace(retval, exit, ERROR, "Failed to  delete lists in DHCPv6 uci config for alias %s", alias);

    retval = uci_call("set", "dhcp", alias, "dhcp", &uci_args, ret);
    if(retval != 0) {
        retval = uci_call("add", "dhcp", alias, "dhcp", &uci_args, ret);
        when_failed_trace(retval, exit, ERROR, "Unable to set dhcp entry in uci of %s.", alias);
    }

    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, ret);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci of %s.", alias);
exit:
    amxc_var_clean(ret);
    amxc_var_clean(&uci_args);
    return retval;
}

/**
 * @brief
 * Disable DHCPv6 pool in the uci config.
 * It also disable DNS and NTP server configuration.
 *
 * @param function_name Name of the function called.
 * @param args Parameters from the amxd pool object.
 * @param ret Return variant.
 * @return int 0 on success.
 */
static int uci_disable_pool(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t* params = NULL;
    const char* alias = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    params = GET_ARG(args, PARAMS);
    when_null_trace(params, exit, ERROR, "Unable to get pool parameters.");
    alias = GET_CHAR(params, "Alias");
    when_null_trace(alias, exit, ERROR, "Unable to get Alias.");

    amxc_var_add_key(cstring_t, &uci_args, "dhcpv6", "disabled");
    amxc_var_add_key(bool, &uci_args, "dns_service", 0);

    retval = uci_call("set", "dhcp", alias, "dhcp", &uci_args, ret);
    when_failed_trace(retval, exit, ERROR, "Unable to set dhcpv6 to disable of %s.", alias);

    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, ret);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci of %s.", alias);
exit:
    amxc_var_clean(&uci_args);
    return retval;
}

/**
 * @brief
 * Start and register functions for the uci module.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dhcpv6_uci_start(void) {
    amxm_module_t* mod = NULL;
    amxm_shared_object_t* so = amxm_so_get_current();

    amxm_module_register(&mod, so, MOD_DHCPV6S_UCI);
    amxm_module_add_function(mod, "enable-pool", uci_enable_pool);
    amxm_module_add_function(mod, "disable-pool", uci_disable_pool);

    return 0;
}
