%populate {
    object 'DHCPv6Server' {
        object 'Pool' {
{% let i=1 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
            instance add('{{lc(Bridge)}}') {
                parameter 'Enable' = true {
                    userflags %usersetting;
                }
                parameter 'Interface' = "Device.Logical.Interface.{{i}}.";
                parameter 'IANAEnable' = true;
{% if ( lc(Bridge) == 'lan' ) : %}
                object 'Option' {
                    instance add('cpe-dhcp-17') {
                        parameter 'Enable' = true;
                        parameter 'Tag' = 17;
                        parameter 'Value' = "$(GW_DHCPV6_OPTION_17_VALUE)";
                    }
                }  
                
{% endif; %}
            }
{% endfor; %}
        }
    }
}

