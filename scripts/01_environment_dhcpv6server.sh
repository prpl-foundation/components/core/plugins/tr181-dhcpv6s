#!/bin/sh
string_to_hex() {
  printf '%s' "$1" | hexdump -ve '/1 "%02X"'
}

if [ -f "/var/etc/environment" ]; then
    source /var/etc/environment
fi

#if enterprise number is empty default to 0000
ENTERPRISE_NUMBER="$PEN"
if [ -z $ENTERPRISE_NUMBER ]; then
    ENTERPRISE_NUMBER=0000
fi

# Convert sub-option values to hexadecimal 4 bytes encoded
ENTERPRISE_NUMBER_HEX=$(echo -n "$ENTERPRISE_NUMBER" | awk '{ printf("%08x\n", $1) }')

# Convert sub-option values to hexadecimal
MANUFACTURER_OUI_HEX=$(string_to_hex "$MANUFACTUREROUI")
SERIAL_NUMBER_HEX=$(string_to_hex "$SERIALNUMBER")
PRODUCT_CLASS_HEX=$(string_to_hex "$PRODUCTCLASS")

MANUFACTURER_OUI_LEN=$(echo -n "$MANUFACTUREROUI" | wc -c | awk '{ printf("%04x\n", $1) }')
SERIAL_NUMBER_LEN=$(echo -n "$SERIALNUMBER" | wc -c | awk '{ printf("%04x\n", $1) }')
PRODUCT_CLASS_LEN=$(echo -n "$PRODUCTCLASS" | wc -c | awk '{ printf("%04x\n", $1) }')


GW_DHCPV6_OPTION_17_VALUE_DATA="0004${MANUFACTURER_OUI_LEN}${MANUFACTURER_OUI_HEX}0005${SERIAL_NUMBER_LEN}${SERIAL_NUMBER_HEX}0006${PRODUCT_CLASS_LEN}${PRODUCT_CLASS_HEX}"
GW_DHCPV6_OPTION_17_VALUE="${ENTERPRISE_NUMBER_HEX}${GW_DHCPV6_OPTION_17_VALUE_DATA}"

echo "export GW_DHCPV6_OPTION_17_VALUE=\"$GW_DHCPV6_OPTION_17_VALUE\"" >> /var/etc/environment
